/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of instruction list
 */

#include "instruction.h"

/**
 * Initializes instruction list.
 *
 * @param TInstructionList** list
 *
 * @return TError ERROR_OK. For error states see helper_allocate_memmory().
 */
TError instruction_init_list(TInstructionList **list)
{
	TError error_state = ERROR_OK;

	error_state = helper_allocate_memmory(
		(void **) list,
		0, // List has no memmory yet
		1, // Single list is enough
		sizeof(TInstructionList)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	(*list)->first = NULL;
	(*list)->last = NULL;
	(*list)->active = NULL;

	return ERROR_OK;
}

/**
 * Clears instruction list.
 *
 * @param TInstructionList** list
 */
void instruction_clear_list(TInstructionList **list)
{
	TInstructionListNode *node;

	if ((*list)->first != NULL) {
		// Go through the list till end
		while ((*list)->first != NULL) {
			// Get current node and shift to next one
			node = (*list)->first;
			(*list)->first = (*list)->first->next;

			// Clear node data
			if (node->data != NULL) {
				// Address 1
				if (node->data->address_1 != NULL) {
					free(node->data->address_1);
					node->data->address_1 = NULL;
				}
				// Address 2
				if (node->data->address_2 != NULL) {
					free(node->data->address_2);
					node->data->address_2 = NULL;
				}
				// Address 3
				if (node->data->address_3 != NULL) {
					free(node->data->address_3);
					node->data->address_3 = NULL;
				}
				// Data
				free(node->data);
			}

			// Clear node
			free(node);
		}
	}

	// Clear list
    if (*list != NULL) {
    	free(*list);
  		*list = NULL;
    }
}

/**
 * Creates new node from given data and inserts it to the end of instruction list.
 * 
 * @param TInstructionList* list
 * @param TInstructionType type
 * @param TInstructionFlag flag
 * @param void* address_1
 * @param void* address_2
 * @param void* address_3
 *
 * @return TError ERROR_OK. For error states see helper_allocate_memmory().
 */
TError instruction_insert_last(
	TInstructionList *list,
	TInstruction * instruction
) {

	TInstructionListNode *node;

	TError error_state = ERROR_OK;

	node = malloc(sizeof(TInstructionListNode));
/*
	error_state = helper_allocate_memmory(
		(void **) &node,
		0, // Node has no memmory yet
		1, // Single node is enough
		sizeof(TInstructionListNode)
	);
*/
	if (error_state != ERROR_OK) {
		return error_state;
	}

	node->data = malloc(sizeof(TInstruction)); 

/*	error_state = helper_allocate_memmory(
		(void **) &(node->data),
		0, // Data has no memmory yet
		1, // Single data item is enough
		sizeof(TInstruction)
	);
*/
	if (error_state != ERROR_OK) {
		return error_state;
	}


	node->data->type = instruction->type;  
	node->data->flag = instruction->flag;
	node->data->var_type = instruction->var_type;

	

	int size = 0;
	if (instruction->address_1 != NULL){
		size = strlen(instruction->address_1);
		node->data->address_1 = malloc(sizeof(char)* size + 1);
		strcpy(node->data->address_1, instruction->address_1); 
	}
	else
		node->data->address_1 = NULL;

	if (instruction->address_2 != NULL){
		size = strlen(instruction->address_2);
		node->data->address_2 = malloc(sizeof(char)* size + 1);
		strcpy(node->data->address_2, instruction->address_2);
	}
	else
		node->data->address_2 = NULL;
	
	if (instruction->address_3 != NULL){
		size = strlen(instruction->address_3);
		node->data->address_3 = malloc(sizeof(char)* size + 1);
		strcpy(node->data->address_3, instruction->address_3); 
	}
	else
		node->data->address_3 = NULL;

	free(instruction->address_1);
	free(instruction->address_2);
	free(instruction->address_3);
	instruction->address_1 = NULL;
	instruction->address_2 = NULL;
	instruction->address_3 = NULL;

	instruction->type = INSTRUCTION_INIT;
	instruction->flag = FALSE;
//	node->data->address_1 = address_1; 
//	node->data->address_2 = address_2; 
//	node->data->address_3 = address_3; 
	
	node->next = NULL;

	if (list->last == NULL) {
		list->first = node;
		list->last = node;
	
	} else {
		list->last->next = node;
		list->last = node;
	}

	return ERROR_OK;
}

/**
 * Activates next or the first instruction.
 * 
 * @param TInstructionList* list
 */
void instruction_activate_next(TInstructionList *list)
{
	if (list->active != NULL) {
		list->active = list->active->next;

	} else {
		list->active = list->first;
	}
}

/**
 * @brief Rewinds to the given instruction.
 *
 * @param TInstructionList* list
 * @param TInstruction* instruction
 */
void instruction_rewind(TInstructionList *list, TInstruction *instruction)
{
	list->active = list->first;
			
	while(true) {

		if ((list->active == NULL)
			|| (list->active->data == instruction)
		) {
			return;
		}

		list->active = list->active->next;
	}
}

/**
 * @brief Rewinds before the given instruction.
 *
 * @param TInstructionList* list
 * @param TInstruction* instruction
 */
void instruction_rewind_before(TInstructionList *list, TInstruction *instruction)
{
	list->active = list->first;
			
	while(true) {

		if ((list->active == NULL)
			|| (list->active->next == NULL)
			|| (list->active->next->data == instruction)
		) {
			return;
		}

		list->active = list->active->next;
	}
}

/**
 * Reads data of active instruction.
 * 
 * @param TInstructionList* list
 *
 * @return TInstruction Data of active instruction, NULL otherwise.
 */
TInstruction *instruction_read(TInstructionList *list)
{
	if (list->active == NULL) {
		return NULL;
	}

	return list->active->data;
}

/**
 * Tells if the list is empty or not.
 *
 * @param TInstructionList* list
 *
 * @return bool True if the list is empty, false otherwise.
 */
bool instruction_is_empty(TInstructionList *list)
{
	if (list->first == NULL) {
		return true;
	}

	return false;
}

/**
 * @brief      Prints instruction record
 *
 * @param      instruction  The instruction
 */
void instruction_print(TInstruction *instruction)
{

	if (instruction != NULL)
	{
		switch (instruction->type){

			case INSTRUCTION_CLASS: 
	            printf("INSTRUCTION_CLASS");
	            break;

			case INSTRUCTION_END_CLASS: 
	            printf("INSTRUCTION_END_CLASS");
	            break;

			case INSTRUCTION_ASSIGNMENT: 
	            printf("INSTRUCTION_ASSIGNMENT");
	            break;
			case INSTRUCTION_OPERAND: 
	            printf("INSTRUCTION_OPERAND");
	            break;

			case INSTRUCTION_WHILE: 
	            printf("INSTRUCTION_WHILE");
	            break;

			case INSTRUCTION_END_WHILE: 
	            printf("INSTRUCTION_END_WHILE");
	            break;

			case INSTRUCTION_IF: 
	            printf("INSTRUCTION_IF");
	            break;

			case INSTRUCTION_END_IF: 
	            printf("INSTRUCTION_END_IF");
	            break;

			case INSTRUCTION_ELSE: 
	            printf("INSTRUCTION_ELSE");
	            break;

			case INSTRUCTION_FUNCTION: 
	            printf("INSTRUCTION_FUNCTION");
	            break;
	        case INSTRUCTION_PARAMS: 
	            printf("INSTRUCTION_PARAMS");
	            break;

	        case INSTRUCTION_LEFT_BRACKET: 
	            printf("INSTRUCTION_LEFT_BRACKET");
	            break;

	        case INSTRUCTION_RIGHT_BRACKET: 
	            printf("INSTRUCTION_RIGHT_BRACKET");
	            break;

	        case INSTRUCTION_END_STATEMENT: 
	            printf("INSTRUCTION_END_STATEMENT");
	            break;

	        case INSTRUCTION_OPERATOR: 
	            printf("INSTRUCTION_OPERATOR");
	            break;

	        case INSTRUCTION_START_STATEMENT: 
	            printf("INSTRUCTION_START_STATEMENT");
	            break;    
	         case INSTRUCTION_CALL_FUNCTION: 
	            printf("INSTRUCTION_CALL_FUNCTION");
	            break; 

			case INSTRUCTION_RETURN: 
	            printf("INSTRUCTION_RETURN");
	            break; 
			case INSTRUCTION_DECLARATION: 
	            printf("INSTRUCTION_DECLARATION");
	            break; 

		}
		printf("\n");

		switch (instruction->var_type)
		{
			case TYPE_UNDEF:
				printf("var_type: TYPE_UNDEF\n");
				break;

			case TYPE_INT:
				printf("var_type: TYPE_INT\n");
				break;

			case TYPE_DOUBLE:
				printf("var_type: TYPE_DOUBLE\n");
				break;

			case TYPE_STRING:
				printf("var_type: TYPE_STRING\n");
				break;

			case TYPE_QUOTED_STRING:
				printf("var_type: TYPE_QUOTED_STRING\n");
				break;

			case TYPE_ID_SIMPLE:
				printf("var_type: TYPE_ID_SIMPLE\n");
				break;

			case TYPE_ID_FULL:
				printf("var_type: TYPE_ID_FULL\n");
				break;
			case TYPE_VOID:
				printf("var_type: TYPE_VOID\n");
				break;
		}

		if (instruction->flag == TRUE)
			printf("flag: TRUE\n");
		else if (instruction->flag == FALSE)
			printf("flag: FALSE\n");
		else
			printf("flag: NULL\n");



		printf("addr1: %s\n",(char*)instruction->address_1);
		printf("addr2: %s\n",(char*)instruction->address_2);
		printf("addr3: %s\n",(char*)instruction->address_3);
		printf("\n");

	}

}



/**
 * @brief      Prints all tokens on instruction list (starts from active)
 *
 * @param      instruction  The instruction
 * @param      list         The list
 */

void instruction_print_all(TInstructionList *list)
{
	TInstruction *instruction = NULL;
	bool run = true;

	if (instruction_is_empty(list)) {
		return;
	}
			
	while(run) {
		instruction_activate_next(list);
		instruction = instruction_read(list);

		if (instruction == NULL) {
			break;
		}
	
		instruction_print(instruction);
	}
}

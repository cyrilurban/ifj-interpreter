/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of IAL algorithms
 * version: b/4/II
 *			b - Boyer-Moore Algorithm
 *			4 - ListMerge Sort
 *			II- Tabulka symbolov implementovana pomocou tabulky s rozptýlenými položkami
 * ial.c
 */

#include "ial.h"

//-------------------------Boyer-Moore Algorithm for string search-------------------------

/**
 * Fisrt heuristic of Boyer-Moore Algorithm
 * 
 * @param char* pattern - pattern for search
 * @param int  array_jump - array where are all the jumps saved
 * @return number of jumps 
 */
void computeJumps(int array_jump[], char *pattern)
{
	int i;
	int pat_length = strlen(pattern);

		for (i = 0; i < MAX_ALPHABET_SIZE; i++)
		{
   			array_jump[i] = pat_length;
		}

   		for (i = 0; i < pat_length - 1; i++)
   		{
   			array_jump[(int)pattern[i]] = pat_length - i - 1;
   		}
    return;  				
}

/**
 * Second heuristic of Boyer-Moore Algorithm
 * 
 * @param char* pattern - pattern for search
 * @param int  array_match - array used for matching
 * @param int  length - length of movement
 * @return length for movement 
 */
void computeMatchJumps(int array_match[], char *pattern)
{
	int k;
	int q;
	int qq;
	int length = strlen(pattern);

	int Backup[length + 1];

	for (q = 0; q < length + 1; q++)
	{
		Backup[q] 	   = 0;
		array_match[q] = 0;
	}
		k = length;
		q = length + 1;

		while (k > 0)
		{
			Backup[k] = q;

				while((q <= length) && (pattern[k - 1] != pattern[q - 1]))
				{
					array_match[q] = (array_match[q] > length - k ? array_match[q] : length - k);
					q = Backup[q]; 
				}

			k = k - 1;
			q = q - 1;
		}

	for (k = 0; k < q; k++)
	{
		array_match[k] = (array_match[k] > length - k ? array_match[q] : length - k);
	}

	qq = Backup[q];

	while (q < length)
	{
		while (q < qq)
		{
			array_match[q] = (array_match[q] > qq - q + length ? array_match[q] : qq - q + length);
			q = q + 1;
		}

		qq = Backup[qq];
	}
	return;
}

/**
 * Compute index of string in text
 * 
 * @param char* pattern 
 * @param char* string
 * @return better algorithm
 */
int BMA(char *string, char *pattern)
{	
  int j;
  int k;
  int str_length = strlen(string);
  int pat_length = strlen(pattern);
  int array_jump[MAX_ALPHABET_SIZE];
  int array_match[pat_length];
  
  
  computeMatchJumps(array_match, pattern);
  computeJumps(array_jump, pattern);

  j = pat_length;
  k = pat_length;
 
  while ((j < str_length) && (k > 0)) 
  {
  	if (string[j] == pattern[k]) 
  	{      
      j--;
      k--;
    } 
    else 
    {                          
       j = j + (array_jump[(int) string[j]] < array_match[k] ? array_jump[(int) string[j]] : array_match[k]);
       k = pat_length;
    }
    
  }

  return (k == 0) ? (j + 1) : 0;
 
}

//-------------------------List Merge Sort Algorithm for sorting linked list-------------------------


/**
 * Divide list
 * 
 * @param list** headRef
 * @return 
 */
struct list* sortMerge(struct list* a, struct list* b);
void frontBackSplit(struct list* source, struct list** frontRef, struct list** backRef);
//zmenime nasledujuci pointer a tym sa pretriedi zoznam
void listMergeSort(struct list** headRef)
{
	struct list* head  = *headRef;
	struct list* a;
	struct list* b;

	if ((head == NULL) || (head->next == NULL))
	{
		return;
	}
	//rozdeli zoznam na 'a' a 'b' "podzoznamy"
	frontBackSplit(head, &a, &b);
	//rekurzivne triedi novovytvorene zoznamy
	listMergeSort(&a);
	listMergeSort(&b);
	//spoji dva triedene zoznamy
	*headRef = sortMerge(a, b);
}

/**
 * Sorting
 * 
 * @param list* a
 * @param list* b
 * @return result
 */
struct list* sortMerge(struct list* a, struct list* b)
{
	struct list* result = NULL;

	if (a == NULL)
	{
		return (b);
	}
	else if (b == NULL)
	{
		return (a);
	}
	//vyberie z jeden zo zoznamov a alebo b
	if (a->data <= b->data)
	{
		result = a;
		result->next = sortMerge(a->next, b);
	}
		else
		{
			result = b;
			result->next = sortMerge(a, b->next);
		}
		return(result);
}

/**
 * Split to front and back 
 * 
 * @param list* source
 * @param list** frontRef
 * @param list** backRef
 * @return 
 */
void frontBackSplit(struct list* source, struct list** frontRef, struct list** backRef)
{
	struct list* fast;
	struct list* slow;

	if (source == NULL || source->next == NULL)
	{
		*frontRef = source;
		*backRef  = NULL; 
	}
		else
		{
			slow = source;
			fast = source->next;
		
			while (fast != NULL)
			{
				fast = fast->next;
					if (fast != NULL)
					{
						slow = slow->next;
						fast = fast->next;
					}
			}
		*frontRef = source;
		*backRef  = slow->next;
		slow->next = NULL;
		}
}


//-------------------------Hash Table -------------------------

/**
 * Spread function 
 * 
 * @param tKey key - key 
 * @return index for key 
 */
int hashCode(tKey key)
{
	int retval = 1;
	int keylen = strlen(key);
	int i;

	for (i = 0; i < keylen; i++ )
	{
		retval += key[i];
	}
	
	return ( retval % HASH_TABLE_SIZE );
}

/**
 * Initializing table
 * 
 * @param tHTable* ptrht - pointer to hash table
 * @return 
 */
void htInit(tHTable* ptrht)
{
	int i;

	if (ptrht == NULL)
	{
		return;
	}	
		for (i = 0; i < HASH_TABLE_SIZE; i++)
		{
			(*ptrht)[i] = NULL;
		}

    return;
}

/**
 * Item searching based on key "key" 
 * 
 * @param tHTable* ptrht
 * @param tKey key
 * @return 
 */
tHTItem* htSearch(tHTable* ptrht, tKey key)
{
	/*zacneme mechanizmom na polozke pola ku ktorej sa dostaneme
	**indexom urcenym z funkce hashCode a potom testujeme ci je volna alebo ne
	*/
	if (ptrht != NULL && key != NULL) 
	{
		tHTItem *p_pom = (*ptrht)[hashCode(key)];
		
		//pokial je polozka obsadena
		while (p_pom != NULL) 
		{
			if (strcmp(p_pom->key, key) == 0) 
			{
				return p_pom;
			} 
			else 
			{
				p_pom = p_pom->ptrnext;
			}
		}

		return NULL;
	} 
	else 
	{
		return NULL;
	}
}

/**
 * Insert item wit key "key" and datas "data" to the table 
 * 
 * @param tHTable* ptrht
 * @param tKey key
 * @param tData data
 * @param tDataType data_type
 * @return 
 */
void htInsert(
	tHTable* ptrht, 
	tKey key, 
	tData data, 
	tData param,
	TInstructionList *list, 
	tDataType data_type)

{

	if (ptrht != NULL || key != NULL) 
	{ 

  	tHTItem *p_pom = htSearch(ptrht, key);
  
  		//prvok sa nasiel a prepise sa obsah 'data'
  		if (p_pom != NULL) 
  		{
   			p_pom->data = data;
   			p_pom->param = param;
   			p_pom->data_type = data_type;
   			p_pom->list = list;
   			return;
  		}

  	//ak sa nasiel alokujeme si miesto na novy kluc a vlozime prvok
  	p_pom = (tHTItem *) malloc(sizeof(tHTItem));
  
 		 if (p_pom == NULL) 
 		 {
   			 return;
  		 }

  		 int index_HT = hashCode(key);

  		 p_pom->ptrnext     = (*ptrht)[index_HT];
		 (*ptrht)[index_HT] = p_pom;

		 		//p_pom->list = list;
		 		p_pom->key  = key;
		 		p_pom->data_type = data_type;
		 		p_pom->list = list;
		 		if (data_type != FUNCTION)
		 			p_pom->data = data;
		 		else{
			 		int size = strlen(data);
			 		p_pom->data =(void*) malloc(sizeof(char)*size +1);
			 		strcpy(p_pom->data, data);
			 	}
			 	p_pom->param = param;
		 return;	

	}

  return;
}

/**
 * Read the value of data part given by key
 * 
 * @param tHTable* ptrht
 * @param tKey key
 * @return 
 */
tData* htRead(tHTable* ptrht, tKey key)
{
	tHTItem *p_pom = htSearch(ptrht, key);

	return (p_pom == NULL) ? NULL : &p_pom->data; 
}

/**
 * Function deletes item with key "key" from ptrht table and free the item correctly 
 * 
 * @param tHTable* ptrht
 * @param tKey key
 * @return 
 */
void htDelete(tHTable* ptrht, tKey key)
{
	if (ptrht != NULL || key != NULL) 
	{

		tHTItem *p_pom = (*ptrht)[hashCode(key)];
		tHTItem *d_pom = NULL;

		while (p_pom != NULL)
		{
			if (strcmp(p_pom->key, key) == 0) 
			{
				if (d_pom == NULL)
				{
					(*ptrht)[hashCode(key)] = p_pom->ptrnext;
				}
					else
					{
						d_pom->ptrnext = p_pom->ptrnext;
					}

			free((void*)p_pom);	
			p_pom = NULL;

			}
				else
				{
					d_pom = p_pom;
					p_pom = p_pom->ptrnext;
				}
		}

		return;
			
	}
		else
		{
			return;
		}		
}

/**
 * All items of ptrht table are destroyed, and free used space 
 * 
 * @param tHTable* ptrht
 *
 * @return 
 */
void htClearAll(tHTable* ptrht)
{
	int i;
	tHTItem *p_pom;
	tHTItem *p_pom2;


	if (ptrht != NULL ) 
	{

		for (i = 0; i < HASH_TABLE_SIZE; i++)
		 {
		 	p_pom = (*ptrht)[i];
		 	
		 	while (p_pom != NULL) 
		 	{
 
      			//Record with class
      			if (p_pom->data_type == HASH_TABLE)
      			{
 					htClearAll(p_pom->data);
      			}
      			
      			if (p_pom->param != NULL)
      			{
      				htClearAll(p_pom->param);
      			}

      			if (p_pom->list != NULL)
      			{
 					instruction_clear_list(&p_pom->list);
      			}

      			if (p_pom->data != NULL)
      			{
      				free(p_pom->data);
      			}

      			p_pom2 = p_pom->ptrnext;
      			//free((void *) p_pom);
				p_pom = p_pom2;      			
		 	}

		 	(*ptrht)[i] = NULL; 
		 }	
	}

	return;
}


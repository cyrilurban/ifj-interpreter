/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of interpreter
 */

#include "interpreter.h"

/**
 * @brief Interprets instruction list.
 *
 * @param TInstructionList* instruction_list
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 *     ERROR_INTERNAL
 *     ERROR_SEMANTICS_DEFINITION
 */
TError interpreter_run(TInstructionList *instruction_list, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;
	TInstruction *instruction = NULL;
	bool run = true;

	if (instruction_is_empty(instruction_list)) {
		return error_state;
	}

	// Check program enter point.
	error_state = interpreter_check_program_enter_point(symbol_table);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	// Remember program enter point.
	bool main_class_exists = false;
	bool is_browsing_main_class = false;
	TInstruction *main_function_instruction = NULL;

	// Symbol table handling.
	tHTable *function_symbol_table = NULL;
	tHTable *class_symbol_table = NULL;
	tHTItem *symbol_table_item = NULL;

	// Statement handling.
	bool is_statement = false;
	TInterpreterStack *statement_stack = NULL;
	TInterpreterStackItem statement_result;

	// Declaration handling.
	bool is_declaration = false;
	TInterpreterStackItem declaration_target;

	// Assignment handling.
	bool is_assignment = false;
	TInterpreterStackItem assignment_target;

	if (error_state != ERROR_OK) {
		return error_state;
	}

	// Full id
	TFullId full_id;


	// Start that pile of shit.
	while (run) {

		instruction_activate_next(instruction_list);
		instruction = instruction_read(instruction_list);

		if (instruction == NULL) {

			// Call main function.
			if (main_function_instruction != NULL) {
				instruction_rewind(instruction_list, main_function_instruction);
				main_function_instruction = NULL;

				instruction = instruction_read(instruction_list);

				// Manually call main function.
				if (instruction != NULL) {
					instruction->type = INSTRUCTION_CALL_FUNCTION;
				}
			}

			// @todo pop context stack and continue after function call

			// The end.
			if ((instruction == NULL) && (main_function_instruction == NULL)) {
				break;
			}
		}

		switch (instruction->type) {

			case INSTRUCTION_INIT:
				break;

			case INSTRUCTION_CLASS:

				// Set active symbol table.
				symbol_table_item = htSearch(symbol_table, instruction->address_1);

				if ((symbol_table_item == NULL)
					|| (symbol_table_item->data_type != HASH_TABLE)
				) {
					return ERROR_INTERNAL;
				}

				class_symbol_table = (tHTable *) symbol_table_item->data;

				// Is it currently browsing main class.
				if (!main_class_exists
					&& (strcmp((char *) instruction->address_1, MAIN_CLASS_NAME) == 0)
				) {
					main_class_exists = true;
					is_browsing_main_class = true;
				}

				break;

			case INSTRUCTION_END_CLASS:
				break;

			case INSTRUCTION_ASSIGNMENT:

				// Declaration assignment
				if (is_declaration && (instruction->address_1 == NULL)) {
					is_assignment = true;
					is_declaration = false;
					break;
				}

				is_assignment = true;

				error_state = interpreter_convert_instruction_to_stack_item(&assignment_target, instruction);

				if (error_state != ERROR_OK) {
					return error_state;
				}

				/** @todo Can be made in parser */
				assignment_target.data_type = (interpreter_is_full_id(assignment_target.data.string))
					? INTERPRETER_STACK_DATA_TYPE_ID_FULL : INTERPRETER_STACK_DATA_TYPE_ID_SIMPLE
				;

				break;

			case INSTRUCTION_DECLARATION:

				is_declaration = true;

				assignment_target.data_type = interpreter_map_data_type_from_instruction(instruction->var_type);
				assignment_target.data.string = (char *) instruction->address_1;


				error_state = interpreter_declare(
					(char *) instruction->address_1,
					instruction->var_type,
					function_symbol_table,
					class_symbol_table
				);

				if (error_state != ERROR_OK) {
					return error_state;
				}

				break;

			case INSTRUCTION_WHILE:

				break;

			case INSTRUCTION_END_WHILE:

				break;

			case INSTRUCTION_IF:

				// is_ifelse = true;

				break;

			case INSTRUCTION_ELSE:
				break;

			case INSTRUCTION_END_IF:
				break;

			case INSTRUCTION_FUNCTION:

				// Remember main function instruction.
				if (is_browsing_main_class
					&& (strcmp((char *) instruction->address_1, MAIN_FUNCTION_NAME) == 0)
				) {
					main_function_instruction = instruction;
					is_browsing_main_class = false;
				}

				break;

			case INSTRUCTION_CALL_FUNCTION:

				// Check embeded function call.
				if (helper_match_pattern((char *) instruction->address_1, EMBEDED_FUNCTION_NAME_PREFIX_REGEX) == ERROR_HELPER_MATCH) {

					if (strcmp((char *) instruction->address_1, EMBEDED_FUNCTION_NAME_READ_INT) == 0) {
						statement_result.data_type = INTERPRETER_STACK_DATA_TYPE_INT;
						error_state = embeded_read_int(&(statement_result.data.number_integer));
					
					} else if (strcmp((char *) instruction->address_1, EMBEDED_FUNCTION_NAME_READ_DOUBLE) == 0) {
						statement_result.data_type = INTERPRETER_STACK_DATA_TYPE_DOUBLE;
						error_state = embeded_read_double(&(statement_result.data.number_double));

					} else if (strcmp((char *) instruction->address_1, EMBEDED_FUNCTION_NAME_READ_STRING) == 0) {
						statement_result.data_type = INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING;
						error_state = embeded_read_string(statement_result.data.string);

					} else if (strcmp((char *) instruction->address_1, EMBEDED_FUNCTION_NAME_PRINT) == 0) {
					
					} else if (strcmp((char *) instruction->address_1, EMBEDED_FUNCTION_NAME_LENGTH) == 0) {

					} else if (strcmp((char *) instruction->address_1, EMBEDED_FUNCTION_NAME_SUBSTR) == 0) {

					} else if (strcmp((char *) instruction->address_1, EMBEDED_FUNCTION_NAME_COMPARE) == 0) {

					} else if (strcmp((char *) instruction->address_1, EMBEDED_FUNCTION_NAME_FIND) == 0) {

					} else if (strcmp((char *) instruction->address_1, EMBEDED_FUNCTION_NAME_SORT) == 0) {

					}

					if (error_state != ERROR_OK) {
						return error_state;
					}

					// Assignment
					if (is_assignment) {
						error_state = interpreter_set_indirect_value(
							assignment_target.data.string,
							&statement_result,
							function_symbol_table,
							class_symbol_table,
							symbol_table
						);

						if (error_state != ERROR_OK) {
							return error_state;
						}

						is_assignment = false;
					}

					// End embeded functions
					break;
				}

				// Create function symbol table
				symbol_table_item = (tHTItem*) malloc ( sizeof(tHTable) );
				symbol_table_item->key = "*UNDEF*";
				symbol_table_item->data = NULL;
				symbol_table_item->ptrnext = NULL;
				function_symbol_table = (tHTable*) malloc (sizeof(tHTable));
				int i;
				for (i = 0; i < HASH_TABLE_SIZE; (*function_symbol_table)[i++] = symbol_table_item);

				if (function_symbol_table == NULL) {
					return ERROR_INTERNAL;
				}

				// Full id .. global table
				if (interpreter_is_full_id((char *) instruction->address_1)) {
					error_state = interpreter_split_full_id(&full_id, (char *) instruction->address_1);

					if (error_state != ERROR_OK) {
						return ERROR_INTERNAL;
					}

					symbol_table_item = htSearch(symbol_table, full_id.namespace);

					if (symbol_table_item == NULL) {
						return ERROR_SEMANTICS_DEFINITION;
					}

					if (symbol_table_item->data_type != HASH_TABLE) {
						return ERROR_INTERNAL;
					}

					symbol_table_item = htSearch((tHTable *) symbol_table_item->data, full_id.id);

					if (symbol_table_item == NULL) {
						return ERROR_SEMANTICS_DEFINITION;
					}

					interpreter_clear_full_id(&full_id);
				
				// Simple id .. class table
				} else {

					symbol_table_item = htSearch(class_symbol_table, (char *) instruction->address_1);

					if (symbol_table_item == NULL) {
						return ERROR_SEMANTICS_DEFINITION;
					}
				}

				// Switch instruction list.
				if ((symbol_table_item != NULL)
					&& (symbol_table_item->data_type == FUNCTION)
					&& (symbol_table_item->list != NULL)
				) {
					instruction_list = symbol_table_item->list;
				}

				break;

			case INSTRUCTION_PARAMS:
				break;

			case INSTRUCTION_RETURN:
				break;

			case INSTRUCTION_LEFT_BRACKET:

				if (is_statement) {
					error_state = interpreter_push_statement_stack(
						statement_stack,
						instruction,
						true
					);

					if (error_state != ERROR_OK) {
						return error_state;
					}
				}

				break;

			case INSTRUCTION_RIGHT_BRACKET:

				if (is_statement) {
					error_state = interpreter_push_statement_stack(
						statement_stack,
						instruction,
						true
					);

					if (error_state != ERROR_OK) {
						return error_state;
					}
				}

				break;

			case INSTRUCTION_START_STATEMENT:

				is_statement = true;

				error_state = interpreter_init_stack(&statement_stack);

				if (error_state != ERROR_OK) {
					return error_state;
				}

				break;

			case INSTRUCTION_OPERAND:
			case INSTRUCTION_OPERATOR:

				error_state = interpreter_push_statement_stack(
					statement_stack,
					instruction,
					(instruction->type == INSTRUCTION_OPERATOR) ? true : false
				);

				if (error_state != ERROR_OK) {
					return error_state;
				}

				break;

			case INSTRUCTION_END_STATEMENT:

				is_statement = false;

				error_state = expression_solve(
					&statement_result,
					statement_stack,
					function_symbol_table,
					class_symbol_table,
					symbol_table
				);

				if (error_state != ERROR_OK) {
					return error_state;
				}

				interpreter_clear_stack(&statement_stack);

				// Assignment
				if (is_assignment) {
					error_state = interpreter_set_indirect_value(
						assignment_target.data.string,
						&statement_result,
						function_symbol_table,
						class_symbol_table,
						symbol_table
					);

					if (error_state != ERROR_OK) {
						return error_state;
					}

					is_assignment = false;
				}

				break;

			default:
				run = false;
				error_state = ERROR_INTERNAL;
				break;
		}

	}

	return error_state;
}

/**
 * @brief Checks existence of program enter point.
 *
 * @param tHTable* symbol_table
 *
 * @return TError ERROR_OK. ERROR_SEMANTICS_DEFINITION if enter point is missing.
 */
TError interpreter_check_program_enter_point(tHTable *symbol_table)
{
	tHTItem *symbol_table_item = htSearch(symbol_table, MAIN_CLASS_NAME);

	// Check existence of main class
	if ((symbol_table_item != NULL)
		&& (symbol_table_item->data_type == HASH_TABLE)
	) {
		symbol_table_item = htSearch(symbol_table_item->data, MAIN_FUNCTION_NAME);

		// Check existence of main function
		if ((symbol_table_item == NULL)
			|| (symbol_table_item->data_type != FUNCTION)
		) {
			return ERROR_SEMANTICS_DEFINITION;
		}

	} else {
		return ERROR_SEMANTICS_DEFINITION;
	}

	return ERROR_OK;
}

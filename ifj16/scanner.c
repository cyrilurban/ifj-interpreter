/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of scanner
 */

#include "scanner.h"

/**
 * @brief Gets next token from the source.
 *
 * @param TToken* token
 * @param TInputSource* source
 *
 * @return TError
 *     ERROR_OK.
 *     ERROR_SCANNER_LEXEME if bad input is detected.
 *     ERROR_INTERNAL in case of omitted state. 
 *     For error states see helper_allocate_memmory() and scanner_append_data().
 */
TError scanner_get_token(TToken *token, TInputSource *source)
{
	TError error_state = ERROR_OK;
	TScannerState state = SCANNER_STATE_INIT;
	int c, i;
	long int escape_sequence_translator_char;
	char escape_sequence_translator_string[SCANNER_OCTAL_ESCAPE_SEQUENCE_LENGTH];
	bool run = true;

	error_state = scanner_initialize_token_data(token);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	while(run) {

		// Synchronize line counter.
		token->line = source->line;

		// Read character
		c = getc(source->input);

		switch (state) {

			case SCANNER_STATE_INIT:

				if (isspace(c)) {

					if (c == '\n') {
						source->line++;
					}
					
					state = SCANNER_STATE_INIT;
				
				// Always test number before id or keyword, because id cannot start with number!
				} else if (isdigit(c)) {
					state = (c == '0') ? SCANNER_STATE_NUMBER_ZERO : SCANNER_STATE_NUMBER;
					error_state = scanner_append_data(token, c);

				// Always test id or keyword after number, because id cannot start with number!
				} else if (isalpha(c) || (c == '_') || (c == '$')) {
					state = SCANNER_STATE_ID_SIMPLE_OR_KEY_WORD;
					ungetc(c, source->input);

				} else if (c == '=') {
					state = SCANNER_STATE_EQUALS;
				
				} else if (c == '>') {
					state = SCANNER_STATE_GREATER;

				} else if (c == '<') {
					state = SCANNER_STATE_LESSER;

				} else if (c == '!') {
					state = SCANNER_STATE_NOT_EQUALS;
					
				} else if (c == '/') {
					state = SCANNER_STATE_SLASH;

				} else if (c == '"') {
					state = SCANNER_STATE_STRING;
					
				} else if (c == '*') {
					token->type = T_Asterisk;
					run = false;

				} else if (c == '{') {
					token->type = T_Left_BBracket;
					run = false;

				} else if (c == '}') {
					token->type = T_Right_BBracket;
					run = false;
					
				} else if (c == '(') {
					token->type = T_Left_Bracket;
					run = false;

				} else if (c == ')') {
					token->type = T_Right_Bracket;
					run = false;
					
				} else if (c == ',') {
					token->type = T_Comma;
					run = false;

				} else if (c == '.') {
					token->type = T_Dot;
					run = false;
					
				} else if (c == '-') {
					token->type = T_Minus;
					run = false;
				
				} else if (c == '+') {
					token->type = T_Plus;
					run = false;

				} else if (c == ';') {
					token->type = T_Semicolon;
					run = false;

				} else if (c == EOF) {
					token->type = T_EOF;
					run = false;

				} else {
					token->type = T_Error;
					return ERROR_SCANNER_LEXEME;
				}

				break;

			case SCANNER_STATE_ID_SIMPLE_OR_KEY_WORD:
				
				if (isalpha(c) || isdigit(c) || (c == '_') || (c == '$')) {
					error_state = scanner_append_data(token, c);

				} else if (c == '.') {
					state = SCANNER_STATE_ID_FULL;
					error_state = scanner_append_data(token, c);

				} else {
					ungetc(c, source->input);

					// Test keywords.
					if (strcmp(token->data, "boolean") == 0) {
						token->type = T_Boolean;
						run = false;

					} else if (strcmp(token->data, "break") == 0) {
						token->type = T_Break;
						run = false;

					} else if (strcmp(token->data, "class") == 0) {
						token->type = T_Class;
						run = false;

					} else if (strcmp(token->data, "continue") == 0) {
						token->type = T_Continue;
						run = false;

					} else if (strcmp(token->data, "do") == 0) {
						token->type = T_Do;
						run = false;

					} else if (strcmp(token->data, "double") == 0) {
						token->type = T_Double;
						run = false;

					} else if (strcmp(token->data, "else") == 0) {
						token->type = T_Else;
						run = false;

					} else if (strcmp(token->data, "false") == 0) {
						token->type = T_False;
						run = false;

					} else if (strcmp(token->data, "for") == 0) {
						token->type = T_For;
						run = false;

					} else if (strcmp(token->data, "if") == 0) {
						token->type = T_If;
						run = false;

					} else if (strcmp(token->data, "int") == 0) {
						token->type = T_Int;
						run = false;

					} else if (strcmp(token->data, "return") == 0) {
						token->type = T_Return;
						run = false;

					} else if (strcmp(token->data, "String") == 0) {
						token->type = T_String;
						run = false;

					} else if (strcmp(token->data, "static") == 0) {
						token->type = T_Static;
						run = false;

					} else if (strcmp(token->data, "true") == 0) {
						token->type = T_True;
						run = false;

					} else if (strcmp(token->data, "void") == 0) {
						token->type = T_Void;
						run = false;

					} else if (strcmp(token->data, "while") == 0) {
						token->type = T_While;
						run = false;

					// Extra "key word" for better validation
					} else if (strcmp(token->data, "Main") == 0) {
						token->type = T_Main;
						run = false;

					// Extra "key word" for better validation
					} else if (strcmp(token->data, "run") == 0) {
						token->type = T_Run;
						run = false;

					} else {
						token->type = T_ID_Simple;
						run = false;
					}
				}

				break;

			case SCANNER_STATE_ID_FULL:

				if (isalpha(c) || (c == '_') || (c == '$')) {
					error_state = scanner_append_data(token, c);

				} else if (isdelimiter(c)) {
					ungetc(c, source->input);
					token->type = T_ID_Full;
					run = false;

				} else {
					token->type = T_Error;
					return ERROR_SCANNER_LEXEME;
				}

				break;

			case SCANNER_STATE_NUMBER:

				if (isdigit(c)) {
					error_state = scanner_append_data(token, c);

				} else if (c == '.') {
					state = SCANNER_STATE_NUMBER_FLOAT;
					error_state = scanner_append_data(token, c);

				} else if ((c == 'E') || (c == 'e')) {
					state = SCANNER_STATE_NUMBER_EXPONENT;
					error_state = scanner_append_data(token, c);

				} else if(isdelimiter(c)) {
					ungetc(c, source->input);
					token->type = T_Number_Int;
					run = false;

				} else {
					token->type = T_Error;
					return ERROR_SCANNER_LEXEME;
				}

				break;

			case SCANNER_STATE_NUMBER_EXPONENT:

				if (isdigit(c)) {
					state = SCANNER_STATE_NUMBER_EXPONENT_VALUE;
					error_state = scanner_append_data(token, c);

				} else if ((c == '+') || (c == '-')) {
					state = SCANNER_STATE_NUMBER_EXPONENT_SIGN;
					error_state = scanner_append_data(token, c);

				} else {
					token->type = T_Error;
					return ERROR_SCANNER_LEXEME;
				}

				break;

			case SCANNER_STATE_NUMBER_EXPONENT_SIGN:

				if (isdigit(c)) {
					state = SCANNER_STATE_NUMBER_EXPONENT_VALUE;
					error_state = scanner_append_data(token, c);

				} else {
					token->type = T_Error;
					return ERROR_SCANNER_LEXEME;
				}

				break;

			case SCANNER_STATE_NUMBER_EXPONENT_VALUE:

				if (isdigit(c)) {
					error_state = scanner_append_data(token, c);

				} else if(isdelimiter(c)) {
					ungetc(c, source->input);
					token->type = T_Number_Double;
					run = false;

				} else {
					token->type = T_Error;
					return ERROR_SCANNER_LEXEME;
				}

				break;

			case SCANNER_STATE_NUMBER_FLOAT:

				if (isdigit(c)) {
					error_state = scanner_append_data(token, c);

				} else if ((c == 'E') || (c == 'e')) {
					state = SCANNER_STATE_NUMBER_EXPONENT;
					error_state = scanner_append_data(token, c);

				} else if(isdelimiter(c)) {
					ungetc(c, source->input);
					token->type = T_Number_Double;
					run = false;

				} else {
					token->type = T_Error;
					return ERROR_SCANNER_LEXEME;
				}

				break;

			case SCANNER_STATE_NUMBER_ZERO:

				if (c == '0') {

				} else if (c == '.') {
					state = SCANNER_STATE_NUMBER_FLOAT;
					error_state = scanner_append_data(token, c);

				} else if(isdelimiter(c)) {
					ungetc(c, source->input);
					token->type = T_Number_Int;
					run = false;

				} else {
					token->type = T_Error;
					return ERROR_SCANNER_LEXEME;
				}

				break;

			case SCANNER_STATE_STRING:

				// Can directly write characters with ASCII number > 31. Except '\"'.
				if ((c > 31) && (c != '\"') && (c != '\\')) {
					error_state = scanner_append_data(token, c);

				} else if (c == '\"') {
					token->type = T_Quoted_String;
					run = false;

				} else if (c == '\\') {
					state = SCANNER_STATE_STRING_ESCAPE_SEQUENCE;

				} else {
					token->type = T_Error;
					return ERROR_SCANNER_LEXEME;
				}

				break;

			case SCANNER_STATE_STRING_ESCAPE_SEQUENCE:

				if ((c == '"') || (c == 'n') || (c == 't') || (c == '\\')) {

					// Translate escape sequence.
					if (c == '"') {
						c = '\"';

					} else if (c == 'n') {
						c = '\n';

					} else if (c == 't') {
						c = '\t';

					} else if (c == '\\') {
						c = '\\';
					}

					state = SCANNER_STATE_STRING;
					error_state = scanner_append_data(token, c);

				} else if ((c == '0') || (c == '1') || (c == '2') || (c == '3')) {
					ungetc(c, source->input);

					// Read octal escape sequence. -1 because of space for terminating null byte ('\0').
					for (i = 0; i < (SCANNER_OCTAL_ESCAPE_SEQUENCE_LENGTH - 1); i++) {
						c = getc(source->input);

						if ((c < '0') || (c > '7')) {
							token->type = T_Error;
							return ERROR_SCANNER_LEXEME;
						}

						escape_sequence_translator_string[i] = c;
					}

					// Convert escape sequence to number
					escape_sequence_translator_string[SCANNER_OCTAL_ESCAPE_SEQUENCE_LENGTH - 1] = '\0';
					escape_sequence_translator_char = strtol(
						escape_sequence_translator_string,
						NULL,
						8 // Converting octal number
					);

					state = SCANNER_STATE_STRING;
					error_state = scanner_append_data(token, (int) escape_sequence_translator_char);

				} else {
					token->type = T_Error;
					return ERROR_SCANNER_LEXEME;
				}

				break;

			case SCANNER_STATE_EQUALS:
				
				if (c == '=') {
					token->type = T_Equals_Equals;
					run = false;
				
				} else {
					ungetc(c, source->input);
					token->type = T_Equals;
					run = false;
				}

				break;

			case SCANNER_STATE_GREATER:

				if (c == '=') {
					token->type = T_Greater_Equals;
					run = false;
				
				} else {
					ungetc(c, source->input);
					token->type = T_Greater;
					run = false;
				}

				break;

			case SCANNER_STATE_LESSER:
				
				if (c == '=') {
					token->type = T_Lower_Equals;
					run = false;

				} else {
					ungetc(c, source->input);
					token->type = T_Lower;
					run = false;
				}
				
				break;

			case SCANNER_STATE_NOT_EQUALS:
				
				if (c == '=') {
					token->type = T_Not_Equals;
					run = false;

				} else {
					token->type = T_Error;
					return ERROR_SCANNER_LEXEME;
				}

				break;

			case SCANNER_STATE_SLASH:

				if (c == '*') {
					state = SCANNER_STATE_COMMENT_BLOCK;
				
				} else if (c == '/') {
					state = SCANNER_STATE_COMMENT_LINE;

				} else {
					ungetc(c, source->input);
					token->type = T_Slash;
					run = false;
				}

				break;

			case SCANNER_STATE_COMMENT_BLOCK:

				if (c == '*') {
					state = SCANNER_STATE_COMMENT_BLOCK_END;
				
				} else if (c == '\n') {
					source->line++;

				} else if (c == EOF) {
					token->type = T_Error;
					return ERROR_SCANNER_LEXEME;
				}

				break;

			case SCANNER_STATE_COMMENT_BLOCK_END:

				if (c == '/') {
					state = SCANNER_STATE_INIT;

				} else if (c == EOF) {
					token->type = T_Error;
					return ERROR_SCANNER_LEXEME;

				} else {
					state = SCANNER_STATE_COMMENT_BLOCK;
				}

				break;

			case SCANNER_STATE_COMMENT_LINE:

				if (c == '\n') {
					state = SCANNER_STATE_INIT;
					source->line++;

				} else if (c == EOF) {
					token->type = T_EOF;
					run = false;
				}

				break;

			// In case of omitted state.
			default:

				token->type = T_Error;
				return ERROR_INTERNAL;

				break;
		}

		// Check success of scanner_append_data().
		if (error_state != ERROR_OK) {
			token->type = T_Error;
			return error_state;
		}
	}

	return ERROR_OK;
}

/**
 * @brief Appends the character to the token data.
 *
 * @param TToken* token
 * @param int character
 *
 * @return TError ERROR_OK. For other error states see helper_allocate_memmory().
 */
TError scanner_append_data(TToken *token, int character)
{
	TError error_state = ERROR_OK;
	char *temporary_data;

	// There is space for one extra character at least.
	if (token->data_length_available >= (token->data_length_full + 1)) {
		token->data[token->data_length_full - 1] = (char) character;
		token->data[token->data_length_full] = '\0';
		token->data_length_full++;

		return ERROR_OK;
	}

	// Initialize for right allocation variant
	temporary_data = token->data;
	token->data = NULL;

	error_state = helper_allocate_memmory(
		(void **) &(token->data),
		0, // Data has no memmory yet
		token->data_length_full + HELPER_ALLOCATE_MEMMORY_SIZE,
		sizeof(char)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	strncpy(token->data, temporary_data, token->data_length_full);
	free(temporary_data);

	token->data_length_available += HELPER_ALLOCATE_MEMMORY_SIZE;
	token->data[token->data_length_full - 1] = (char) character;
	token->data[token->data_length_full] = '\0';
	token->data_length_full++;

	return ERROR_OK;
}

/**
 * @brief Initializes the token.
 *
 * @param TToken** token
 *
 * @return TError ERROR_OK. For other error states see helper_allocate_memmory().
 */
TError scanner_initialize_token(TToken **token)
{
	TError error_state = ERROR_OK;

	// Initialize token for right allocation variant.
	*token = NULL;

	error_state = helper_allocate_memmory(
		(void **) token,
		0, // Token has no memmory yet
		1, // Single token
		sizeof(TToken)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	(*token)->type = T_Error;
	(*token)->data = NULL;
	(*token)->data_length_full = 0;
	(*token)->data_length_available = 0;
	(*token)->line = 0;

	return ERROR_OK;
}

/**
 * @brief Initializes token data.
 *
 * @param TToken* token
 *
 * @return TError ERROR_OK. For other error states see helper_allocate_memmory().
 */
TError scanner_initialize_token_data(TToken *token)
{
	TError error_state = ERROR_OK;

	// Initialize data for right allocation variant.
	if (token->data != NULL) {
		if (token->data_length_available > 0) {
			memset(token->data, '\0', token->data_length_available);
			token->data_length_full = 1;
			return ERROR_OK;

		} else {
			free(token->data);
			token->data = NULL;
			token->data_length_full = 0;
			token->data_length_available = 0;
		}
	}

	error_state = helper_allocate_memmory(
		(void **) &(token->data),
		0, // Data has no memmory yet
		-1, // Use default value
		sizeof(char)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	token->type = T_Error;
	token->data[0] = '\0';
	token->data_length_full = 1;
	token->data_length_available = HELPER_ALLOCATE_MEMMORY_SIZE;
	token->line = 0;

	return ERROR_OK;
}

/**
 * @brief Cleares memmory taken by the token.
 *
 * @param TToken** token
 */
void scanner_clear_token(TToken **token)
{
	if ((*token)->data != NULL) {
		free((*token)->data);
	}

	free(*token);
	*token = NULL;
}

/**
 * @brief Copies token data to the data copy.
 *
 * @param TToken* token
 * @param char* data_copy
 * 
 * @return TError ERROR_OK. For other error states see helper_allocate_memmory().
 */
TError scanner_copy_token_data(TToken *token, char *data_copy)
{
	TError error_state = ERROR_OK;

	if (token->data == NULL) {
		data_copy = NULL;
		return ERROR_OK;
	}

	error_state = helper_allocate_memmory(
		(void **) &(data_copy),
		0, // Data copy has no memmory yet
		token->data_length_full,
		sizeof(char)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	strncpy(data_copy, token->data, token->data_length_full);

	return ERROR_OK;
}

/**
 * @brief Tell if given character can be considered as delimiter.
 *
 * @param char c
 *
 * @return bool True if character is delimiter, false otherwise.
 */
bool isdelimiter(char c)
{
	if(strchr(SCANNER_DELIMITER_CHARACTERS, c) || isspace(c)) {
		return true;
	}

	return false;
}

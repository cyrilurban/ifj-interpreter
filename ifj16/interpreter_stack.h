/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of interpreter stack
 */

#ifndef INTERPRETER_STACK_H
#define INTERPRETER_STACK_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "embeded.h"
#include "error.h"
#include "helper.h"
#include "ial.h"
#include "instruction.h"

/** @def Regular expression describing full id. */
#define INTERPRETER_STACK_FULL_ID_REGEX "^([A-Za-z]|_|\\$)([A-Za-z0-9]|_|\\$)*\\.([A-Za-z]|_|\\$)([A-Za-z0-9]|_|\\$)*$"

/**
 * @brief Interpreter stack item data structure.
 */
typedef union TInterpreterStackData {
	bool boolean;
	double number_double;
	int number_integer;
	char *string;
	tHTable *symbol_table;
	struct TInterpreterStack *stack;
	TInstruction *instruction;
} TInterpreterStackData;

/**
 * @brief Interpreter stack item data types.
 */
typedef enum TInterpreterStackDataType {
	INTERPRETER_STACK_DATA_TYPE_EMPTY = 0,
	INTERPRETER_STACK_DATA_TYPE_UNDEF,
	INTERPRETER_STACK_DATA_TYPE_INT,
	INTERPRETER_STACK_DATA_TYPE_DOUBLE,
	INTERPRETER_STACK_DATA_TYPE_STRING,
	INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING,
	INTERPRETER_STACK_DATA_TYPE_ID_SIMPLE,
	INTERPRETER_STACK_DATA_TYPE_ID_FULL,
	INTERPRETER_STACK_DATA_TYPE_VOID,
	INTERPRETER_STACK_DATA_TYPE_BOOLEAN,
	INTERPRETER_STACK_DATA_TYPE_OPERATOR,
	INTERPRETER_STACK_DATA_TYPE_SYMBOL_TABLE,
	INTERPRETER_STACK_DATA_TYPE_STACK,
	INTERPRETER_STACK_DATA_TYPE_INSTRUCTION
} TInterpreterStackDataType;

/**
 * @brief Interpreter stack item structure.
 */
typedef struct TInterpreterStackItem {
	TInterpreterStackData data;
	TInterpreterStackDataType data_type;
} TInterpreterStackItem;

/**
 * @brief Interpreter stack structure.
 */
typedef struct TInterpreterStack {
	int size;
	int size_inverted;
	int size_available;
	TInterpreterStackItem *stack;
} TInterpreterStack;

/**
 * @brief Initializes interpreter stack.
 *
 * @param TInterpreterStack** stack
 *
 * @return TError ERROR_OK. For other error state see helper_allocate_memmory().
 */
TError interpreter_init_stack(TInterpreterStack **stack);

/**
 * @brief Cleares interpreter stack.
 *
 * @param TInterpreterStack** stack
 */
void interpreter_clear_stack(TInterpreterStack **stack);

/**
 * @brief Pushes the item to the stack.
 *
 * @param TInterpreterStack* stack
 * @param TInterpreterStackItem item
 *
 * @return TError ERROR_OK. For other error states see helper_allocate_memmory().
 */
TError interpreter_push_stack(TInterpreterStack *stack, TInterpreterStackItem item);

/**
 * @brief Pushes the item to bottom of the stack.
 *
 * @param TInterpreterStack* stack
 * @param TInterpreterStackItem item
 *
 * @return TError ERROR_OK. ERROR_INTERNAL if stack is empty.
 */
TError interpreter_push_inverted_stack(TInterpreterStack *stack, TInterpreterStackItem item);

/**
 * @brief Gets the item from top of the stack.
 *
 * @param TInterpreterStack* stack
 *
 * @return TInterpreterStackItem In case stack is empty, item.data_type is set to INTERPRETER_STACK_DATA_TYPE_EMPTY.
 */
TInterpreterStackItem interpreter_pop_stack(TInterpreterStack *stack);

/**
 * @brief Gets the item from bottom of the stack.
 *
 * @param TInterpreterStack* stack
 *
 * @return TInterpreterStackItem In case stack is empty, item.data_type is set to INTERPRETER_STACK_DATA_TYPE_EMPTY.
 */
TInterpreterStackItem interpreter_pop_inverted_stack(TInterpreterStack *stack);

/**
 * @brief Reads the item from top of the stack.
 *
 * @param TInterpreterStack* stack
 *
 * @return TInterpreterStackItem In case stack is empty, item.data_type is set to INTERPRETER_STACK_DATA_TYPE_EMPTY.
 */
TInterpreterStackItem interpreter_read_stack(TInterpreterStack *stack);

/**
 * @brief Tells if stack is empty.
 *
 * @param TInterpreterStack* stack
 *
 * @return bool True if stack is empty, false otherwise.
 */
bool interpreter_is_empty_stack(TInterpreterStack *stack);

/**
 * @brief Tells if stack is empty.
 *
 * @param TInterpreterStack* stack
 *
 * @return bool True if stack is empty, false otherwise.
 */
bool interpreter_is_empty_inverted_stack(TInterpreterStack *stack);

/**
 * @brief Tells if stack is full.
 *
 * @param TInterpreterStack* stack
 *
 * @return bool True if stack is full, false otherwise.
 */
bool interpreter_is_full_stack(TInterpreterStack *stack);

/**
 * @brief Tells if stack is full.
 *
 * @param TInterpreterStack* stack
 *
 * @return bool True if stack is full, false otherwise.
 */
bool interpreter_is_full_inverted_stack(TInterpreterStack *stack);

/**
 * @brief Translates instruction data types to interpreter stack data types.
 *
 * @param TInstructionVariable type
 *
 * @return TInterpreterStackDataType
 */
TInterpreterStackDataType interpreter_map_data_type_from_instruction(TInstructionVariable type);

/**
 * @brief Converts instruction to stack item.
 *
 * @param TInterpreterStackItem* item
 * @param TInstruction* instruction
 *
 * @return TError ERROR_OK. For other error states see helper_string_to_integer() and helper_string_to_double().
 */
TError interpreter_convert_instruction_to_stack_item(TInterpreterStackItem *item, TInstruction *instruction);

/**
 * @brief Pushes the instruction to the statement stack.
 *
 * @param TInterpreterStack* stack
 * @param TInstruction* instruction
 * @param bool is_operator
 *
 * @return TError ERROR_OK. For other error states see interpreter_convert_instruction_to_stack_item() and interpreter_push_stack().
 */
TError interpreter_push_statement_stack(TInterpreterStack *stack, TInstruction *instruction, bool is_operator);

/**
 * @brief Prints stack.
 *
 * @param TInterpreterStack* stack
 * @param TInterpreterStack* stack
 */
void interpreter_print_stack(TInterpreterStack *stack, bool is_inverted);

/**
 * @brief Full id structure.
 */
typedef struct TFullId {
	char* namespace;
	char* id;
} TFullId;

/**
 * @brief Splits given full id onto namespace and id.
 *
 * @param TFullId* splitted_id
 * @param char* id
 *
 * @return TError
 *     ERROR_OK
 *     ERROR_INTERNAL If given id does not match full id pattern.
 *     For other error states see embeded_substr().
 */
TError interpreter_split_full_id(TFullId *splitted_id, char *id);

/**
 * @brief Cleares content of full id.
 *
 * @param TFullId* id
 */
void interpreter_clear_full_id(TFullId *id);

/**
 * @brief Tells if given id is full or not.
 *
 * @param char* id
 * 
 * @return bool True if given id is full id, false otherwise.
 */
bool interpreter_is_full_id(char *id);

/**
 * @brief Tells if given data type is id.
 *
 * @param TInterpreterStackDataType type
 * 
 * @return bool True if given data type is id, false otherwise.
 */
bool interpreter_has_data_type_id(TInterpreterStackDataType type);

/**
 * @brief Translates symbol table data types to interpreter stack data types.
 *
 * @param tDataType type
 *
 * @return TInterpreterStackDataType
 */
TInterpreterStackDataType interpreter_map_data_type_from_symbol_table(tDataType type);

/**
 * @brief Loads value from symbol table.
 *
 * @param char* id
 * @param TInterpreterStackItem* value
 * @param tHTable* function_symbol_table
 * @param tHTable* class_symbol_table
 * @param tHTable* symbol_table
 *
 * @return TError
 */
TError interpreter_load_indirect_value(char *id, TInterpreterStackItem *value, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);

/**
 * @brief Loads value from symbol table.
 *
 * @param char* id
 * @param TInterpreterStackItem* value
 * @param tHTable* function_symbol_table
 * @param tHTable* class_symbol_table
 * @param tHTable* symbol_table
 *
 * @return TError
 */
TError interpreter_set_indirect_value(char *id, TInterpreterStackItem *value, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);

/**
 * @brief Loads value from symbol table.
 *
 * @param char* id
 * @param TInstructionVariable* type
 * @param tHTable* function_symbol_table
 * @param tHTable* class_symbol_table
 *
 * @return TError
 */
TError interpreter_declare(char *id, TInstructionVariable type, tHTable *function_symbol_table, tHTable *class_symbol_table);

/**
 * @brief Prints value of stack item based on its type.
 *
 * @pram TInterpreterStackItem* value
 */
void interpreter_print_value_based_on_type(TInterpreterStackItem *value);

#endif

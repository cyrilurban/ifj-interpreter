/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/*
 * Implementation of parser
 */

#include "parser.h"

/** 
 * @var int bracket
 *
 * @brief bracket
 *
 * global variable - bracket:
 * mechanism to control the same number of left and right bracket in the one statement
 * T_Left_Bracket : bracket++;
 * T_Right_Bracket : bracket--;
 * after end statement must be bracket == 0;
 */
int bracket = 0;
tHTItem* tmpht;
tHTable* ptrht;
tKey activeClass;
tKey activeFunction;

/**
 * @brief      parser
 *
 * @param      File  The file
 *
 * @return     TError
 */
TError parser(TInputSource* File){
	TError error_state = ERROR_OK;
	TToken *tokenOld = NULL;
	TToken *tokenNew = NULL;

	error_state = scanner_initialize_token(&tokenOld);
	error_state = scanner_initialize_token(&tokenNew);

	if (error_state != ERROR_OK) {
		return ERROR_INTERNAL;
	}

	tmpht = (tHTItem*) malloc ( sizeof(tHTable) );
	tmpht->key = "*UNDEF*";
	tmpht->data = NULL;
	tmpht->ptrnext = NULL;
	ptrht = (tHTable*) malloc ( sizeof(tHTable) );
	int i;
	for ( i=0; i<HASH_TABLE_SIZE; (*ptrht)[i++] = tmpht );


	TInstructionList *list = NULL;
	instruction_init_list(&list);

	TInstruction * instruction;
	instruction = malloc(sizeof(TInstruction));
	instruction->type = INSTRUCTION_CLASS;
	instruction->flag = FALSE;
	instruction->var_type = TYPE_UNDEF;
	instruction->address_1 = NULL;
	instruction->address_2 = NULL;
	instruction->address_3 = NULL;

	error_state = sim_next_class(instruction, list, tokenNew, tokenOld, File);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	error_state = interpreter_run(list, ptrht);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	htClearAll(ptrht);
	//parser never clears instruction lists, just for testing leaks
	instruction_clear_list(&list);
	scanner_clear_token(&tokenOld);
	scanner_clear_token(&tokenNew);
	//TODO: test releasing memmory -> valgrind
	free(instruction->address_1);
	free(instruction->address_2);
	free(instruction->address_3);
	free(instruction);
	free(activeClass);
	free(activeFunction);
	return error_state;

}


/**
 * @brief      Copy token data
 *
 * @param      token  The token
 *
 * @return     Pointer to copied data
 */

void * copy_data(TToken* token){
	int size = strlen(token->data);
	
	char * string = malloc((sizeof(char) * size) +1);
	
	strcpy(string, token->data);

	return (void*) string;
}

/**
 * @brief      Copy character
 *
 * @param[in]  c     Character to copy
 *
 * @return     Pointer on copied char
 */
void * copy_character(char c){

	char * charPtr = malloc(sizeof(char)+1);

	*charPtr = c;

	return (void *) charPtr;
}
/**
 * @brief      Class 
 *
 * @param      token  The token
 * @param      File   The file
 *
 * @return     TError
 * 
 * <class> -> T_Class T_ID || T_Left_BBracket <static_definitions> 
 * T_Right_BBracket <next_class>
 */
TError sim_class(
	TInstruction * instruction, 
	TInstructionList *list, 
	TToken* tokenNew, 
	TToken *tokenOld, 
	TInputSource * File)
{
	TError error_state = ERROR_OK;

	if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK) //T_Left_BBracket
		return error_state;


	if (tokenOld->type != T_Left_BBracket){
		return ERROR_PARSER_SYNTAX;
	}

	error_state = sim_static_definitions(instruction, list, tokenNew, tokenOld, File); // <static_definitions>

	if (error_state != ERROR_OK)
		return error_state;                                                           

	// token pre-loaded in "sim_static_definitions"// T_Right_BBracket
	if (tokenOld->type != T_Right_BBracket)
		return ERROR_PARSER_SYNTAX;

	// INSTRUCTION_END_CLASS
	instruction->type = INSTRUCTION_END_CLASS;
	instruction_insert_last(list, instruction);


	error_state = sim_next_class(instruction, list, tokenNew, tokenOld, File);
		
	return error_state;

}


/**
 * @brief      Next class
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * <next_class> -> T_EOF // end of file, without syntax errors
 * <next_class> -> <class>
 * <next_class> -> <next_class>
 */
TError sim_next_class(
	TInstruction * instruction, 
	TInstructionList *list, 
	TToken* tokenNew, 
	TToken *tokenOld, 
	TInputSource * File){
	TError error_state = ERROR_OK;
	
	if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK) //T_Class || T_EOF
		return error_state;	

	if (tokenOld->type == T_Class){
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;//T_EOF || T_ID_Simple


		if (tokenOld->type == T_ID_Simple || T_Main){//T_ID_Simple || T_Main
			
			// INSTRUCTION_CLASS
			instruction->address_1 = copy_data(tokenOld);
			instruction->type = INSTRUCTION_CLASS;
			int size = strlen(instruction->address_1);
			free(activeClass);
			activeClass = malloc(sizeof(char)*size +1);
			strcpy(activeClass, instruction->address_1);
			instruction_insert_last(list, instruction);


			tHTable* ptrht_tmp = (tHTable*) malloc ( sizeof(tHTable) );	
			char * className = copy_data(tokenOld);
			htInsert(ptrht, className, ptrht_tmp, NULL, NULL, HASH_TABLE);
			//free(className);
			
			if((error_state = sim_class(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK){
				return error_state;
			}
		}
	}
	else if (tokenOld->type == T_EOF){ //T_EOF
		return error_state;
	}
	else	
		return ERROR_PARSER_SYNTAX;
}


/**
 * @brief      Static definitions
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * <static_definitions> -> epsilon
 * <static_definitions> -> T_Static <type> T_ID <end_part_static>
 * 
 * 
 * // static definitions (0-N)
 * <static_definitions> -> epsilon
 * <static_definitions> -> T_Static <type> T_ID <end_part_static>
 * 
 * <end_part_static> -> T_Semicolon <static_definitions> // ;
 * <end_part_static> -> T_Equals <statement> T_Semicolon <static_definitions> // = <statement>;
 * 
 * // function
 * <end_part_static> -> T_Left_BBracket <params> T_Right_Bracket T_Left_BBracket 
 * <function_body> T_Right_BBracket <static_definitions>
 */
TError sim_static_definitions(
	TInstruction * instruction, 
	TInstructionList *list, 
	TToken* tokenNew, 
	TToken *tokenOld, 
	TInputSource * File)
{
	TError error_state = ERROR_PARSER_SYNTAX;


	if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
		return error_state;// T_Static || epsilon

	if (tokenOld->type == T_Static) { // <static_definitions> -> T_Static <type> T_ID_Simple <end_part_static>
			
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;// T_Void || T_Int || T_Double || T_String
		if((error_state = scanner_get_token(tokenNew, File)) != ERROR_OK) 
			return error_state;// T_ID_Simple || T_Run
		
		if ( (error_state = sim_type(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK ) // <type>	
			return error_state;
		else if (tokenNew->type == T_ID_Simple || tokenNew->type == T_Run) {

			//name of variable or function
			char * idName = copy_data(tokenNew);
			//serach for class in table
			tmpht = htSearch(ptrht,activeClass);

			if (tokenOld->type == T_Void) {
				htInsert(tmpht->data, idName, NULL, NULL, NULL, T_VOID);
				instruction->var_type = TYPE_VOID;
			}
			if (tokenOld->type == T_Int) {
				htInsert(tmpht->data, idName, NULL, NULL, NULL, INTEGER);
				instruction->var_type = TYPE_INT;
			}
			if (tokenOld->type == T_Double) {
				htInsert(tmpht->data, idName, NULL, NULL, NULL, DOUBLE);
				instruction->var_type = TYPE_DOUBLE;
			}
			if (tokenOld->type == T_String) {
				htInsert(tmpht->data, idName, NULL, NULL, NULL, STRING);
				instruction->var_type = TYPE_STRING;
			}

			//free(idName);
			// INSTRUCTION_FUNCTION - name of function
			instruction->address_1 = copy_data(tokenNew);

		  	// <end_part_static>
			if ( (error_state = sim_end_part_static(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK ) 
				return error_state;

			else 
				return ERROR_OK; //return with pre-loaded token
		}
		else
			return ERROR_PARSER_SYNTAX;
	}
	else  // <static_definitions> -> epsilon
    	return ERROR_OK; // return with pre-loaded token                                        	
}

/**
 * @brief      Statement definiton - second part 
 * 				(end part static)
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 */
TError sim_end_part_static(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File){
	TError error_state = ERROR_PARSER_SYNTAX;
	tDataType type;

	if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
		return error_state;// T_Semicolon || T_Equals || T_Left_BBracket

	if (tokenOld->type == T_Semicolon) { // T_Semicolon
	    // <static_definitions>
		if ((error_state = sim_static_definitions(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;
		else
			return ERROR_OK;                                                               
	}

	else if (tokenOld->type == T_Equals) { // T_Equals
	                                       
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;// <statement>

		// INSTRUCTION_ASSIGNMENT
		instruction->type = INSTRUCTION_ASSIGNMENT;
		instruction->flag = TRUE;
		instruction->address_1 = copy_data(tokenNew);
		instruction_insert_last(list, instruction);
		instruction->var_type = TYPE_UNDEF;

		if ( (error_state = sim_statement(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;
		
		// token is allready loaded (token is full)

		// T_Semicolon		                          
		if (tokenOld->type != T_Semicolon)
			return ERROR_PARSER_SYNTAX;

		// <static_definitions>	                                                          
		if ( (error_state = sim_static_definitions(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;
		else
			return ERROR_OK;

	}

	else if (tokenOld->type == T_Left_Bracket) { //T_Left_Bracket
		

		// INSTRUCTION_FUNCTION
		instruction->type = INSTRUCTION_FUNCTION;
		int size = strlen(instruction->address_1);
		free(activeFunction);
		activeFunction = malloc(sizeof(char)*size +1);
		strcpy(activeFunction, instruction->address_1);
		instruction_insert_last(list, instruction);
		instruction->var_type = TYPE_UNDEF;

		tHTable* ptrht_tmp = (tHTable*) malloc ( sizeof(tHTable) );
		tmpht = htSearch(ptrht, activeClass);

		htInsert(tmpht->data, activeFunction, NULL, ptrht_tmp, NULL, FUNCTION);

		if (tmpht->data_type == INTEGER){
			tmpht = htSearch(ptrht, activeClass);
			htInsert(tmpht->data, activeFunction, "f",ptrht_tmp, NULL, INTEGER);
		}
		if (tmpht->data_type == STRING){
			tmpht = htSearch(ptrht, activeClass);
			htInsert(tmpht->data, activeFunction, "f",ptrht_tmp, NULL, STRING);
		}
		if (tmpht->data_type == DOUBLE){
			tmpht = htSearch(ptrht, activeClass);
			htInsert(tmpht->data, activeFunction, "f",ptrht_tmp, NULL, DOUBLE);
		}
		if (tmpht->data_type == T_VOID){
			tmpht = htSearch(ptrht, activeClass);
			htInsert(tmpht->data, activeFunction, "f",ptrht_tmp, NULL, T_VOID);
		}

		// <params>
		if ( (error_state = sim_params_definitions(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK){
			return error_state;
		}

		TInstructionList *list_function = NULL;
		instruction_init_list(&list_function);
		instruction->var_type = TYPE_UNDEF;
		
		//switch to function instruction list
		tmpht = htSearch(ptrht, activeClass);
		tmpht = htSearch(tmpht->data, activeFunction);
		tmpht->list = list_function;		


		if (tokenOld->type != T_Right_Bracket)
			return ERROR_PARSER_SYNTAX;

		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;// T_Left_BBracket		                          
		if (tokenOld->type != T_Left_BBracket)
			return ERROR_PARSER_SYNTAX;


		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;// <function_body>
		if ((error_state = sim_function_body(instruction, list_function, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;


	    // tokekn allready loaded from sim_function_body	                          
		if (tokenOld->type != T_Right_BBracket)
			return ERROR_PARSER_SYNTAX;

		// <static_definitions>		                                                               
		if ((error_state = sim_static_definitions(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;

		return ERROR_OK; 		
	}
	else // bad token
		return ERROR_PARSER_SYNTAX;
}


/**
 * @brief      Types
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * <type> -> T_Int
 * <type> -> T_Double
 * <type> -> T_String
 * <type> -> T_Void
 */
TError sim_type(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File){
	TError error_state = ERROR_PARSER_SYNTAX;

	//scanner_get_token(tokenOld, File); // T_Int || T_Double  || T_String  || T_Void

	if (tokenOld->type == T_Int)
		return ERROR_OK;

	else if (tokenOld->type == T_Double)
		return ERROR_OK;

	else if (tokenOld->type == T_String)
		return ERROR_OK;

	else if (tokenOld->type == T_Void)
		return ERROR_OK;

	return error_state;
}


/**
 * @brief      Types without void
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * <type> -> T_Int
 * <type> -> T_Double
 * <type> -> T_String
 * <type> -> T_Void
 */

TError sim_type_without_void(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File){
	TError error_state = ERROR_PARSER_SYNTAX;

	if (tokenOld->type == T_Int)
		return ERROR_OK;

	else if (tokenOld->type == T_Double)
		return ERROR_OK;

	else if (tokenOld->type == T_String)
		return ERROR_OK;

	return error_state;
}


/**
 * @brief      Params
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * <params> -> epsilon
 * <params> -> <type> T_ID_Simple <end_part_params>
 */
TError sim_params_call_function(
	TInstruction * instruction, 
	TInstructionList *list, 
	TToken* tokenNew, 
	TToken *tokenOld, 
	TInputSource * File)
{ //<params>
	TError error_state = ERROR_PARSER_SYNTAX;
	tDataType type;
	// INSTRUCTION_PARAMS
	instruction->type = INSTRUCTION_PARAMS;

	if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
		return error_state;// <type> || T_ID_Simple 	

	// operand
	if (sim_operand(instruction, list, tokenNew, tokenOld, File) == ERROR_OK) {

		// INSTRUCTION_PARAMS: first param
		instruction->address_1 = copy_data(tokenOld);

		if (tokenOld->type == T_Number_Int)
			instruction->var_type = TYPE_INT;
		if (tokenOld->type == T_Number_Double)
			instruction->var_type = TYPE_DOUBLE;
		if (tokenOld->type == T_Quoted_String)
			instruction->var_type = TYPE_QUOTED_STRING;
		
		if (tokenOld->type == T_ID_Simple)
			instruction->var_type = TYPE_ID_SIMPLE;
		if (tokenOld->type == T_ID_Full)
			instruction->var_type = TYPE_ID_FULL;

		// <end_part_params> ; in the function is scanner_get_token
		if ((error_state = sim_end_part_params_call_function(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;
		else
			return ERROR_OK; // epsilon; scanner_get_token is full
	}
	
	// INSTRUCTION_PARAMS: zero params
	instruction_insert_last(list, instruction);
	return ERROR_OK; // epsilon; scanner_get_token is full
	
}


/**
 * @brief      Params
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * <params> -> epsilon
 * <params> -> <type> T_ID_Simple <end_part_params>
 */
TError sim_params_definitions(
	TInstruction * instruction, 
	TInstructionList *list, 
	TToken* tokenNew, 
	TToken *tokenOld, 
	TInputSource * File)
{ //<params>
	TError error_state = ERROR_PARSER_SYNTAX;
	tDataType type;
	// INSTRUCTION_PARAMS
	instruction->type = INSTRUCTION_PARAMS;

	if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
		return error_state;// <type> || T_ID_Simple 	

	// <type>
	if (sim_type_without_void(instruction, list, tokenNew, tokenOld, File) == ERROR_OK) {

		tmpht = htSearch(ptrht, activeClass);
		tmpht = htSearch(tmpht->data, activeFunction);

		if (tokenOld->type == T_Int){
			type = PARAM_INTEGER;
			instruction->var_type = TYPE_INT;
		}
		if (tokenOld->type == T_Double){
			type = PARAM_DOUBLE;
			instruction->var_type = TYPE_DOUBLE;
		}
		if (tokenOld->type == T_String){
			type = PARAM_STRING;
			instruction->var_type = TYPE_STRING;
		}


		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;// T_ID_Simple
		if (tokenOld->type != T_ID_Simple)
			return ERROR_PARSER_SYNTAX;
		
		char * varName = copy_data(tokenOld);
		htInsert(tmpht->param, varName, NULL, NULL, NULL, type);

		//free(varName);
		// INSTRUCTION_PARAMS: first param
		instruction->address_1 = copy_data(tokenOld);

		// <end_part_params> ; in the function is scanner_get_token
		if ((error_state = sim_end_part_params_definitions(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;
		else{			
			return ERROR_OK; // epsilon; scanner_get_token is full
		}
	}

	// INSTRUCTION_PARAMS: zero params
	instruction_insert_last(list, instruction);
	instruction->var_type = TYPE_UNDEF;
	return ERROR_OK; // epsilon; scanner_get_token is full
	
}

/**
 * @brief      Second part of params
 * 				(end part params)
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * <end_part_params> -> epsilon
 * <end_part_params> -> T_Comma T_ID_Simple <end_part_params>
 */
TError sim_end_part_params_definitions(
	TInstruction * instruction, 
	TInstructionList *list, 
	TToken* tokenNew, 
	TToken *tokenOld, 
	TInputSource * File)
{ // <end_part_params>
	TError error_state = ERROR_PARSER_SYNTAX;
	tDataType type;

	// INSTRUCTION_PARAMS
	instruction->type = INSTRUCTION_PARAMS;

	if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
		return error_state;// T_Comma || T_Plus

	if (tokenOld->type == T_Comma || tokenOld->type == T_Plus) {

		if (tokenOld->type == T_Comma) {
			// INSTRUCTION_PARAMS: second param			
			instruction->address_2 = copy_character(',');
		}
		else {
			// INSTRUCTION_PARAMS: second param
			instruction->address_2 = copy_character('+');
		}

		// INSTRUCTION_PARAMS:
		instruction->flag = TRUE;
		instruction_insert_last(list, instruction);
		instruction->var_type = TYPE_UNDEF;

		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;// <type>  || T_ID_Simple	               

		if(sim_type_without_void(instruction, list, tokenNew, tokenOld, File) != ERROR_OK){
			return ERROR_PARSER_SYNTAX;
		}
		else{

			tmpht = htSearch(ptrht, activeClass);
			tmpht = htSearch(tmpht->data, activeFunction);

			if (tokenOld->type == T_Int){
				type = INTEGER;
				instruction->var_type = TYPE_INT;
			}
			if (tokenOld->type == T_Double){
				type = DOUBLE;
				instruction->var_type = TYPE_DOUBLE;
			}
			if (tokenOld->type == T_String){
				type = STRING;
				instruction->var_type = TYPE_STRING;
			}
			
			// behinde <type> can be only ID (not others operands)
			if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
				return error_state;// T_ID_Simple
			if (tokenOld->type != T_ID_Simple)
				return ERROR_PARSER_SYNTAX;
			
			char * varName = copy_data(tokenOld);
			
			htInsert(tmpht->param, varName, NULL, NULL, NULL,type);
		
			free(varName);
		}	
		
		// INSTRUCTION_PARAMS:
		instruction->address_1 = copy_data(tokenOld);

		error_state = sim_end_part_params_definitions(instruction, list, tokenNew, tokenOld, File); // <end_part_params>
		if (error_state != ERROR_OK)
			return error_state;

		return ERROR_OK; // scanner_get_token is full
	}
	else {

		// INSTRUCTION_PARAMS: last param, flag - false
		instruction->flag = FALSE;
		instruction_insert_last(list, instruction);
		instruction->var_type = TYPE_UNDEF;
		return ERROR_OK; // epsilon, scanner_get_token is full
	}
}

/**
 * @brief      Second part of params
 * 				(end part params)
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * <end_part_params> -> epsilon
 * <end_part_params> -> T_Comma T_ID_Simple <end_part_params>
 */
TError sim_end_part_params_call_function(
	TInstruction * instruction, 
	TInstructionList *list, 
	TToken* tokenNew, 
	TToken *tokenOld, 
	TInputSource * File)
{ // <end_part_params>
	TError error_state = ERROR_PARSER_SYNTAX;
	tDataType type;

	// INSTRUCTION_PARAMS
	instruction->type = INSTRUCTION_PARAMS;

	if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
		return error_state;// T_Comma || T_Plus

	if (tokenOld->type == T_Comma || tokenOld->type == T_Plus) {

		if (tokenOld->type == T_Comma) {
			// INSTRUCTION_PARAMS: second param			
			instruction->address_2 = copy_character(',');
		}
		else {
			// INSTRUCTION_PARAMS: second param
			instruction->address_2 = copy_character('+');
		}

		// INSTRUCTION_PARAMS:
		instruction->flag = TRUE;
		instruction_insert_last(list, instruction);
		instruction->var_type = TYPE_UNDEF;


		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;// <type>  || T_ID_Simple	                                                          
		
		// operand
		else if ((error_state = sim_operand(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)                         
			return error_state;

		if (tokenOld->type == T_Number_Int)
			instruction->var_type = TYPE_INT;
		if (tokenOld->type == T_Number_Double)
			instruction->var_type = TYPE_DOUBLE;
		if (tokenOld->type == T_Quoted_String)
			instruction->var_type = TYPE_QUOTED_STRING;
		
		if (tokenOld->type == T_ID_Simple)
			instruction->var_type = TYPE_ID_SIMPLE;
		if (tokenOld->type == T_ID_Full)
			instruction->var_type = TYPE_ID_FULL;

		instruction->address_1 = copy_data(tokenOld);

		error_state = sim_end_part_params_call_function(instruction, list, tokenNew, tokenOld, File); // <end_part_params>
		if (error_state != ERROR_OK)
			return error_state;

		return ERROR_OK; // scanner_get_token is full
	}
	else {

		// INSTRUCTION_PARAMS: last param, flag - false
		instruction->flag = FALSE;

		if (tokenOld->type == T_Number_Int)
			instruction->var_type = TYPE_INT;
		if (tokenOld->type == T_Number_Double)
			instruction->var_type = TYPE_DOUBLE;
		if (tokenOld->type == T_Quoted_String)
			instruction->var_type = TYPE_QUOTED_STRING;

		instruction_insert_last(list, instruction);
		instruction->var_type = TYPE_UNDEF;
		return ERROR_OK; // epsilon, scanner_get_token is full
	}
}

/**
 * @brief      Compoud command
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * <compoud_command> -> T_Left_BBracket || <command_from_compud> T_Right_BBracket <function_body>
 * 
 */
TError sim_compoud_command(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File){ // <end_part_params>
	TError error_state = ERROR_PARSER_SYNTAX;

	if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
		return error_state;
	// if
	// <command> -> T_If T_Left_Bracket <statement> T_Right_Bracket <compoud_command> T_Else <compoud_command> <function_body>
	if (tokenOld->type == T_If) {

		// INSTRUCTION_IF
		instruction->type = INSTRUCTION_IF;
		instruction_insert_last(list, instruction);

		// T_Left_Bracket
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;
		if (tokenOld->type != T_Left_Bracket)
			return ERROR_PARSER_SYNTAX;
		
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;// <statement>
		if ((error_state = sim_statement(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;

		// token is allready loaded (token is full)

		// T_Right_Bracket
		if (tokenOld->type != T_Right_Bracket)
			return ERROR_PARSER_SYNTAX;

		// <sim_compoud_command>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;
		if ((error_state = sim_compoud_command(instruction, list, tokenNew, tokenOld, File) != ERROR_OK)) 
			return error_state;
		// token is allready loaded (token is full)

		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;

		// T_Else
		if (tokenOld->type != T_Else)
			return ERROR_PARSER_SYNTAX;

		// INSTRUCTION_ELSE
		instruction->type = INSTRUCTION_ELSE;
		instruction_insert_last(list, instruction);

		// <sim_compoud_command>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;

		if ((error_state = sim_compoud_command(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK) 
			return error_state;
		// token is allready loaded (token is full)
		
		// INSTRUCTION_END_IF
		instruction->type = INSTRUCTION_END_IF;
		instruction_insert_last(list, instruction);

		if ((error_state = sim_compoud_command(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;
		else	
			return ERROR_OK; // token is allready loaded (token is full)
	}

	// while
	// <command> -> T_While T_Left_Bracket <statement> T_Right_Bracket <compoud_command> <function_body>	
	else if (tokenOld->type == T_While) {

		// INSTRUCTION_WHILE
		instruction->type = INSTRUCTION_WHILE;
		instruction_insert_last(list, instruction);

		// T_Left_Bracket
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;
		if (tokenOld->type != T_Left_Bracket)
			return ERROR_PARSER_SYNTAX;
		

		// <statement>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;
		if ((error_state = sim_statement(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;

		// token is allready full

		// T_Right_Bracket
		if (tokenOld->type != T_Right_Bracket)
			return ERROR_PARSER_SYNTAX;

		// INSTRUCTION_END_WHILE
		instruction->type = INSTRUCTION_END_WHILE;
		instruction_insert_last(list, instruction);

		// <sim_compoud_command>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;
		if ((error_state = sim_compoud_command(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK ) 
			return error_state;
		// token is allready loaded (token is full)

		if ((error_state = sim_compoud_command(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
			return error_state;
		else	
			return ERROR_OK; // token is allready loaded (token is full)
	}

	// return
	// <command> -> T_Return <statement> T_Semicolon
	// <command> -> T_Return T_Semicolon	
	else if (tokenOld->type == T_Return) {

		// INSTRUCTION_RETURN
		instruction->type = INSTRUCTION_RETURN;
		instruction->flag = TRUE;

		// T_Semicolon || <statement>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;
		if (tokenOld->type == T_Semicolon) {
			
			// INSTRUCTION_RETURN
			instruction->flag = FALSE;
			instruction_insert_last(list, instruction);

			if ((error_state = sim_compoud_command(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
				return error_state;
			else	
				return ERROR_OK; // token is allready loaded (token is full)
		}
		else if (sim_statement(instruction, list, tokenNew, tokenOld, File) == ERROR_OK) {

			// token is allready loaded (token is full)
			// T_Semicolon
			if (tokenOld->type != T_Semicolon)
				return ERROR_PARSER_SYNTAX;
			else {

				if ((error_state = sim_compoud_command(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
					return error_state;
				else	
					return ERROR_OK; // token is allready loaded (token is full)
			}
		}
							
	}

	// initialization / assignment || call function (four way, how call)
	// <command> -> T_ID T_Equals <statement> T_Semicolon <function_body>
	//
	// <command> -> T_ID T_Equals T_ID T_Dot T_ID T_Left_Bracket <params> T_Right_Bracket T_Semicolon <function_body>
	// <command> -> T_ID T_Equals T_ID T_Left_Bracket <params> T_Right_Bracket T_Semicolon <function_body> 
	// <command> -> T_ID T_Dot T_ID T_Left_Bracket <params> T_Right_Bracket T_Semicolon <function_body>
	// 
	// <command> -> T_ID T_Left_Bracket <params> T_Right_Bracket T_Semicolon <function_body>
	
	// T_ID
	else if (tokenOld->type == T_ID_Simple || tokenOld->type == T_ID_Full) {

		// INSTRUCTION_ASSIGNMENT
		instruction->type = INSTRUCTION_ASSIGNMENT;
		instruction->address_1 = copy_data(tokenOld);
		
		// must have tmp_instruction, to save unaccomplished instruction
		TInstruction * tmp_instruction;
		tmp_instruction = malloc(sizeof(TInstruction));
		tmp_instruction->address_1 = instruction->address_1;

		// T_Equals || T_Dot || T_Left_Bracket
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;

		// T_Equals
		if (tokenOld->type == T_Equals) {	          

			free(tmp_instruction);

			// INSTRUCTION_ASSIGNMENT
			instruction_insert_last(list, instruction);

			// <statement>

			if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
				return error_state; 	                                                          
 			if ((error_state = sim_statement(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
 				return error_state;

			// token is allready loaded (token is full)

			// T_Semicolon
			if (tokenOld->type != T_Semicolon)
				return ERROR_PARSER_SYNTAX;
			
			// sim_compoud_command				
			if ((error_state = sim_compoud_command(instruction, list, tokenNew, tokenOld, File)) != error_state )
				return error_state;
 			else	
 				return ERROR_OK; // token is allready loaded (token is full)

 		}
		// T_Left_Bracket
		else if (tokenOld->type == T_Left_Bracket) {
			
			//INSTRUCTION_CALL_FUNCTION
			instruction->type = INSTRUCTION_CALL_FUNCTION;
			instruction->address_1 = tmp_instruction->address_1; // before token from tmp
			free(tmp_instruction);
			instruction_insert_last(list, instruction);

			// <params>
			if ((error_state = sim_params_call_function(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
				return error_state;

			// T_Right_Bracket
			if (tokenOld->type != T_Right_Bracket)
				return ERROR_PARSER_SYNTAX;

			// T_Semicolon
			if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
				return error_state;
			if (tokenOld->type != T_Semicolon)
				return ERROR_PARSER_SYNTAX;

			// sim_compoud_command
			if ((error_state = sim_compoud_command(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
				return error_state;
			else	
				return ERROR_OK; // token is allready loaded (token is full)
		}

		// bad token
		else
			return ERROR_PARSER_SYNTAX;
	}
	// definitions (starts type) not allowed in compoud command -> synatx error
	else if ((error_state = sim_type(instruction, list, tokenNew, tokenOld, File)) == ERROR_OK){
		return ERROR_PARSER_SYNTAX;
	}

	// T_Right_BBracket
	if (tokenOld->type == T_Right_BBracket){
		return ERROR_OK;
	}

}


/**
 * @brief      Statement
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * <statement> -> <brackets> <operand> <brackets>
 * <statement> -> <brackets> <operand> <brackets> <operator> <brackets> <operand> <end_part_statement>
 *  
 */
TError sim_statement(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File){ // <end_part_params>
	TError error_state = ERROR_PARSER_SYNTAX;
	// expect, that token is allready load 

	// mechanism to control the same number of left and right bracket in the one statement
	// T_Left_Bracket : bracket++;
	// T_Right_Bracket : bracket--;
	// after end statement must be bracket == 0;
	bracket = 0; // (reset global variable - new statement) 

	// must be here the part from function body (command):
	// <command> -> T_ID T_Dot T_ID T_Left_Bracket <params> T_Right_Bracket T_Semicolon <function_body>
	// <command> -> T_ID T_Left_Bracket <params> T_Right_Bracket T_Semicolon <function_body> 

	// INSTRUCTION_RETURN
	if (instruction->type == INSTRUCTION_RETURN) {
		instruction->flag = TRUE;
		instruction_insert_last(list, instruction);
	}
	
	// <comand>
	// T_ID
	if (tokenOld->type == T_ID_Simple || tokenOld->type == T_ID_Full) {
		
		// INSTRUCTION_OPERAND || INSTRUCTION_FUNCTION
		instruction->type = INSTRUCTION_OPERAND;
		instruction->address_1 = copy_data(tokenOld);

		if (tokenOld->type == T_ID_Full){
			instruction->var_type = TYPE_ID_FULL;
		} 
		else {
			instruction->var_type = TYPE_ID_SIMPLE;
		}

		// must have tmp_instruction, to save unaccomplished instruction
		TInstruction * tmp_instruction;
		tmp_instruction = malloc(sizeof(TInstruction));
		tmp_instruction->address_1 = instruction->address_1;
		tmp_instruction->var_type = instruction->var_type;

		// T_Left_Bracket
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;

		// T_Left_Bracket
		if (tokenOld->type == T_Left_Bracket) {

			free(tmp_instruction);
			
			// INSTRUCTION_CALL_FUNCTION
			instruction->type = INSTRUCTION_CALL_FUNCTION;
			instruction_insert_last(list, instruction);

			// <params>
			if ((error_state = sim_params_call_function(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
				return error_state;

			// T_Right_Bracket
			if (tokenOld->type != T_Right_Bracket)
				return ERROR_PARSER_SYNTAX;

			// <function_body>
			if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
				return error_state;
			if ((error_state = sim_function_body(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
				return error_state;
			else	
				return ERROR_OK; // token is allready loaded (token is full)
		}

		else { 

			// INSTRUCTION_START_STATEMENT
			instruction->type = INSTRUCTION_START_STATEMENT;
			instruction->address_1 = NULL;
			instruction->var_type = TYPE_UNDEF;
			instruction_insert_last(list, instruction);


			// INSTRUCTION_OPERAND
			instruction->type = INSTRUCTION_OPERAND;
			instruction->address_1 = tmp_instruction->address_1; // first operand
			instruction->var_type = tmp_instruction->var_type;
			free(tmp_instruction);
			instruction_insert_last(list, instruction);
			instruction->var_type = TYPE_UNDEF;

			// <operator>
			if (sim_operator(instruction, list, tokenNew, tokenOld, File) == ERROR_OK) {
				
				// <brackets>
				if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
					return error_state;

				if ((error_state = sim_brackets(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
					return error_state;
				// token is allready loaded (token is full)
				
				// more right bracket, then left, it could be next part of function body
				// for example end of while or if - right bracket	
				if (bracket == -1) {

					// INSTRUCTION_END_STATEMENT
					instruction->type = INSTRUCTION_END_STATEMENT;
					instruction_insert_last(list, instruction);

					bracket = 0; // reset global variable
					return ERROR_OK; // token is allready loaded (token is full)				
				}

				// right bracket imidietly before <operand> - ERROR
				if (tokenOld->type == T_Right_Bracket)
					return ERROR_PARSER_SYNTAX;

				// <operand>
				if ((error_state = sim_operand(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
					return error_state;

				if (tokenOld->type == T_Number_Int)
					instruction->var_type = TYPE_INT;
				if (tokenOld->type == T_Number_Double)
					instruction->var_type = TYPE_DOUBLE;
				if (tokenOld->type == T_Quoted_String)
					instruction->var_type = TYPE_QUOTED_STRING;
				if (tokenOld->type == T_ID_Simple){
					instruction->var_type = TYPE_ID_SIMPLE;
				}
				if (tokenOld->type == T_ID_Full){
					instruction->var_type = TYPE_ID_FULL;
				}

				// INSTRUCTION_OPERAND
				instruction->type = INSTRUCTION_OPERAND;
				instruction->address_1 = copy_data(tokenOld);
				instruction_insert_last(list, instruction);

				instruction->var_type = TYPE_UNDEF;

				// <sim_end_part_statemen>
				if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
					return error_state;

				// left bracket after <operand> - ERROR
				if (tokenOld->type == T_Left_Bracket)
					return ERROR_PARSER_SYNTAX;

				if ((error_state = sim_end_part_statement(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
					return error_state;
				else {

					// the same number of left and rigt bracket
					if (bracket == 0) {

						// INSTRUCTION_END_STATEMENT
						instruction->type = INSTRUCTION_END_STATEMENT;
						instruction_insert_last(list, instruction);

						return ERROR_OK; // token is allready loaded (token is full)				
					}
					else
						return ERROR_PARSER_SYNTAX;
				}
			}
			else	
				// the same number of left and rigt bracket
				if (bracket == 0) {

					// INSTRUCTION_END_STATEMENT
					instruction->type = INSTRUCTION_END_STATEMENT;
					instruction_insert_last(list, instruction);

					return ERROR_OK; // token is allready loaded (token is full)				
				}
				else
					return ERROR_PARSER_SYNTAX;
		}
	}
	
	// <statement>
	// INSTRUCTION_START_STATEMENT
	instruction->type = INSTRUCTION_START_STATEMENT;
	instruction_insert_last(list, instruction);

	// **first bracket is right - ERROR
	if (tokenOld->type == T_Right_Bracket)
		return ERROR_PARSER_SYNTAX;

	// <brackets>
	if ((error_state = sim_brackets(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
		return error_state;
	// token is allready loaded (token is full)

	// more right bracket, then left, it could be next part of function body
	// for example end of while or if - right bracket	
	if (bracket == -1) {

		// INSTRUCTION_END_STATEMENT
		instruction->type = INSTRUCTION_END_STATEMENT;
		instruction_insert_last(list, instruction);

		bracket = 0; // reset global variable
		return ERROR_OK; // token is allready loaded (token is full)				
	}
	
	// **right bracket imidietly before <operand> - ERROR
	if (tokenOld->type == T_Right_Bracket)
		return ERROR_PARSER_SYNTAX;

	// <operand>
	if ((error_state = sim_operand(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
		return error_state;


	// INSTRUCTION_OPERAND
	if (tokenOld->type == T_Number_Int){
		instruction->var_type = TYPE_INT;
	}
	if (tokenOld->type == T_Number_Double){
		instruction->var_type = TYPE_DOUBLE;
	}
	if (tokenOld->type == T_Quoted_String){
		instruction->var_type = TYPE_QUOTED_STRING;
	}
	if (tokenOld->type == T_ID_Simple){
		instruction->var_type = TYPE_ID_SIMPLE;
	}
	if (tokenOld->type == T_ID_Full){
		instruction->var_type = TYPE_ID_FULL;
	}

	instruction->type = INSTRUCTION_OPERAND;
	instruction->address_1 = copy_data(tokenOld);
	instruction_insert_last(list, instruction);

	instruction->var_type = TYPE_UNDEF;

	//defauil incialization for next instructions
	instruction->var_type = TYPE_UNDEF;

	// <brackets>
	if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
		return error_state;

	// left bracket after <operand> - ERROR
	if (tokenOld->type == T_Left_Bracket)
		return ERROR_PARSER_SYNTAX;

	if ((error_state = sim_brackets(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
		return error_state;
	// token is allready loaded (token is full)
	
	// more right bracket, then left, it could be next part of function body
	// for example end of while or if - right bracket	
	if (bracket == -1) {

		// INSTRUCTION_END_STATEMENT
		instruction->type = INSTRUCTION_END_STATEMENT;
		instruction_insert_last(list, instruction);

		bracket = 0; // reset global variable
		return ERROR_OK; // token is allready loaded (token is full)				
	}
		

	// <operator>
	if (sim_operator(instruction, list, tokenNew, tokenOld, File) == ERROR_OK) {
		
		// <brackets>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;

		if ((error_state = sim_brackets(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;
		// token is allready loaded (token is full)
		
		// more right bracket, then left, it could be next part of function body
		// for example end of while or if - right bracket	
		if (bracket == -1) {

			// INSTRUCTION_END_STATEMENT
			instruction->type = INSTRUCTION_END_STATEMENT;
			instruction_insert_last(list, instruction);

			bracket = 0; // reset global variable
			return ERROR_OK; // token is allready loaded (token is full)				
		}

		// right bracket imidietly before <operand> - ERROR
		if (tokenOld->type == T_Right_Bracket)
			return ERROR_PARSER_SYNTAX;

		// <operand>
		if ((error_state = sim_operand(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;

		if (tokenOld->type == T_Number_Int){
			instruction->var_type = TYPE_INT;
		}
		if (tokenOld->type == T_Number_Double){
			instruction->var_type = TYPE_DOUBLE;
		}
		if (tokenOld->type == T_Quoted_String){
			instruction->var_type = TYPE_QUOTED_STRING;
		}
		if (tokenOld->type == T_ID_Simple){
			instruction->var_type = TYPE_ID_SIMPLE;
		}
		if (tokenOld->type == T_ID_Full){
			instruction->var_type = TYPE_ID_FULL;
		}

		// INSTRUCTION_OPERAND
		instruction->type = INSTRUCTION_OPERAND;
		instruction->address_1 = copy_data(tokenOld);
		instruction_insert_last(list, instruction);

		instruction->var_type = TYPE_UNDEF;

		// <sim_end_part_statemen>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;

		// left bracket after <operand> - ERROR
		if (tokenOld->type == T_Left_Bracket)
			return ERROR_PARSER_SYNTAX;

		if ((error_state = sim_end_part_statement(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;
		else {

			// the same number of left and rigt bracket
			if (bracket == 0) {

				// INSTRUCTION_END_STATEMENT
				instruction->type = INSTRUCTION_END_STATEMENT;
				instruction_insert_last(list, instruction);

				return ERROR_OK; // token is allready loaded (token is full)				
			}
			else
				return ERROR_PARSER_SYNTAX;
		}
	}
	else	



		// the same number of left and rigt bracket
		if (bracket == 0) {

			// INSTRUCTION_END_STATEMENT
			instruction->type = INSTRUCTION_END_STATEMENT;
			instruction_insert_last(list, instruction);

			return ERROR_OK; // token is allready loaded (token is full)				
		}
		else
			return ERROR_PARSER_SYNTAX;
}


/**
 * @brief      Statemen - second part
 *				(end part statement)
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * <end_part_statement> -> epsilon
 * <end_part_statement> -> <brackets> 
 * <end_part_statement> -> <brackets> <operator> <brackets> <operand> <brackets> <end_part_statement>
 *  
 */
TError sim_end_part_statement(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File){ // <end_part_params>
	TError error_state = ERROR_PARSER_SYNTAX;

	// expect, that token is allready load 
	
	if ((error_state = sim_brackets(instruction, list, tokenNew, tokenOld, File)) == ERROR_OK) {

		// token is allready loaded (token is full)

		// more right bracket, then left, it could be next part of function body
		// for example end of while or if - right bracket	
		if (bracket == -1) {	
			bracket = 0; // reset global variable
			return ERROR_OK; // token is allready loaded (token is full)				
		}

		// <operator>
		else if ((error_state = sim_operator(instruction, list, tokenNew, tokenOld, File)) == ERROR_OK) { 

			// <brackets>
			if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
				return error_state;
			if ((error_state = sim_brackets(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK) 
				return error_state;

			// token is allready loaded (token is full)
			
			// more right bracket, then left, it could be next part of function body
			// for example end of while or if - right bracket	
			if (bracket == -1) {
				bracket = 0; // reset global variable
				return ERROR_OK; // token is allready loaded (token is full)				
			}

			// <operand>
			if ((error_state = sim_operand(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
				return error_state;

			if (tokenOld->type == T_Number_Int){
				instruction->var_type = TYPE_INT;
			}
			if (tokenOld->type == T_Number_Double){
				instruction->var_type = TYPE_DOUBLE;
			}
			if (tokenOld->type == T_Quoted_String){
				instruction->var_type = TYPE_QUOTED_STRING;
			}
			if (tokenOld->type == T_ID_Simple){
				instruction->var_type = TYPE_ID_SIMPLE;
			}
			if (tokenOld->type == T_ID_Full){
				instruction->var_type = TYPE_ID_FULL;
			}

			// INSTRUCTION_OPERAND
			instruction->type = INSTRUCTION_OPERAND;
			instruction->address_1 = copy_data(tokenOld);
			instruction_insert_last(list, instruction);

			instruction->var_type = TYPE_UNDEF;

			// <sim_brackets>
			if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
				return error_state;

			// left bracket after <operand> - ERROR
			if (tokenOld->type == T_Left_Bracket)
				return ERROR_PARSER_SYNTAX;

			if ((error_state = sim_brackets(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK) 
				return error_state;

			// token is allready loaded (token is full)
			
			// more right bracket, then left, it could be next part of function body
			// for example end of while or if - right bracket	
			if (bracket == -1) {
				bracket = 0; // reset global variable
				return ERROR_OK; // token is allready loaded (token is full)				
			}

			// <end_part_statement>
			if ((error_state = sim_end_part_statement(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
				return error_state;
			else
				return ERROR_OK; // token is allready loaded (token is full)
		}

		//  <end_part_statement> -> <brackets> (epsilon)
		else {		
			return ERROR_OK; // token is allready loaded (token is full)
		}
	}
	else
		return error_state;
	// token is allready loaded (token is full)
}


/**
 * @brief      Brackets
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * <brackets> -> epsilon
 * <brackets> -> T_Left_Bracket <brackets>
 * <brackets> -> T_Right_Bracket <brackets>
 *  
 */
TError sim_brackets(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File){ // <end_part_params>
	TError error_state = ERROR_PARSER_SYNTAX;

	// expect, that token is allready load 

	// mechanism to control the same number of left and right bracket in the one statement
	// T_Left_Bracket : bracket++;
	// T_Right_Bracket : bracket--;
	// after end statement must be bracket == 0;

	// T_Left_Bracket
	if (tokenOld->type == T_Left_Bracket) { 
				
		// INSTRUCTION_LEFT_BRACKET
		instruction->type = INSTRUCTION_LEFT_BRACKET;
		instruction->address_1 = copy_character('(');
		instruction_insert_last(list, instruction);
		
		bracket++; // left bracket, increment global variable

		// <brackets>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state; 

		// right bracket imidietly after left bracket - ERROR
		if (tokenOld->type == T_Right_Bracket)
			return ERROR_PARSER_SYNTAX;

		if ((error_state = sim_brackets(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
			return error_state;
		else	
			return ERROR_OK; // token is allready loaded (token is full)
	}
	// T_Right_Bracket
	else if (tokenOld->type == T_Right_Bracket) { 
		
		bracket--; // right bracket, decrement global variable

		// more right bracket, then left, it could be next part of function body
		// for example end of while or if - right bracket	
		if (bracket == -1) {
			return ERROR_OK; // token is allready loaded (token is full)r
		}

		// INSTRUCTION_RIGHT_BRACKET
		instruction->type = INSTRUCTION_RIGHT_BRACKET;
		instruction->address_1 = copy_character(')');
		instruction_insert_last(list, instruction);

		// <brackets>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state; 
		if ((error_state = sim_brackets(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
			return error_state;
		else	
			return ERROR_OK; // token is allready loaded (token is full)
	}
	// <brackets> -> epsilon
	else {
		return ERROR_OK; // token is allready loaded (token is full)
	}

	// token is allready loaded (token is full)
}


/**
 * @brief      Operand
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * operands
 * <operand> -> T_ID
 * <operand> -> T_Number_Int
 * <operand> -> T_Number_Double
 * 
 * <operand> -> T_Quoted_String    	// string literal
 *  
 */
TError sim_operand(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File){ // <end_part_params>
	TError error_state = ERROR_PARSER_SYNTAX;

	// expect, that token is allready load 

	// T_ID
	if (tokenOld->type == T_ID_Simple) { 
		return ERROR_OK; // token is not loaded (token is empty)
	}
	if (tokenOld->type == T_ID_Full) { 
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// T_Number_Int
	else if (tokenOld->type == T_Number_Int) { 
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// T_Number_Double
	else if (tokenOld->type == T_Number_Double) { 
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// T_Quoted_String
	else if (tokenOld->type == T_Quoted_String) { 
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// token is not loaded (token is empty)
}

/**
 * @brief      Operators
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * operators
 * <operator> -> T_Asterisk 		// *
 * <operator> -> T_Slash 			// /
 * <operator> -> T_Plus 			// +
 * <operator> -> T_Minus 			// -
 * <operator> -> T_Equals 			// =
 * <operator> -> T_Lower 			// <
 * <operator> -> T_Greater 			// >
 * <operator> -> T_Lower_Equals 	// <=
 * <operator> -> T_Greater_Equals 	// >=
 * <operator> -> T_Equals_Equals 	// ==
 * <operator> -> T_Not_Equals 		// !=
 *  
 */
TError sim_operator(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File){ // <end_part_params>
	TError error_state = ERROR_PARSER_SYNTAX;

	// expect, that token is allready load 

	// T_Asterisk
	if (tokenOld->type == T_Asterisk) { 
		// INSTRUCTION_OPERATOR
		instruction->type = INSTRUCTION_OPERATOR;
		instruction->address_1 = copy_character('*');
		instruction_insert_last(list, instruction);
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// T_Slash
	else if (tokenOld->type == T_Slash) { 
		// INSTRUCTION_OPERATOR
		instruction->type = INSTRUCTION_OPERATOR;
		instruction->address_1 = copy_character('/');
		instruction_insert_last(list, instruction);
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// T_Plus
	else if (tokenOld->type == T_Plus) { 
		// INSTRUCTION_OPERATOR
		instruction->type = INSTRUCTION_OPERATOR;
		instruction->address_1 = copy_character('+');
		instruction_insert_last(list, instruction);
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// T_Minus
	else if (tokenOld->type == T_Minus) { 
		// INSTRUCTION_OPERATOR
		instruction->type = INSTRUCTION_OPERATOR;
		instruction->address_1 = copy_character('-');
		instruction_insert_last(list, instruction);
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// T_Equals
	else if (tokenOld->type == T_Equals) { 
		// INSTRUCTION_OPERATOR
		instruction->type = INSTRUCTION_OPERATOR;
		instruction->address_1 = copy_character('=');
		instruction_insert_last(list, instruction);
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// T_Lower
	else if (tokenOld->type == T_Lower) { 
		// INSTRUCTION_OPERATOR
		instruction->type = INSTRUCTION_OPERATOR;
		instruction->address_1 = copy_character('<');
		instruction_insert_last(list, instruction);
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// T_Greater
	else if (tokenOld->type == T_Greater) { 
		// INSTRUCTION_OPERATOR
		instruction->type = INSTRUCTION_OPERATOR;
		instruction->address_1 = copy_character('>');
		instruction_insert_last(list, instruction);
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// T_Lower_Equals
	else if (tokenOld->type == T_Lower_Equals) { 
		// INSTRUCTION_OPERATOR
		instruction->type = INSTRUCTION_OPERATOR;

		instruction->address_1 = malloc(sizeof(char)+1);
		char* Lower_Equals = "<=";
		strcpy(instruction->address_1, Lower_Equals);

		instruction_insert_last(list, instruction);
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// T_Greater_Equals
	else if (tokenOld->type == T_Greater_Equals) { 
		// INSTRUCTION_OPERATOR
		instruction->type = INSTRUCTION_OPERATOR;
		instruction->address_1 = malloc(sizeof(char)+1);
		char* Lower_Equals = ">=";
		strcpy(instruction->address_1, Lower_Equals);
		instruction_insert_last(list, instruction);
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// T_Equals_Equals
	else if (tokenOld->type == T_Equals_Equals) { 
		// INSTRUCTION_OPERATOR
		instruction->type = INSTRUCTION_OPERATOR;
		instruction->address_1 = malloc(sizeof(char)+1);
		char* Lower_Equals = "==";
		strcpy(instruction->address_1, Lower_Equals);
		instruction_insert_last(list, instruction);
		return ERROR_OK; // token is not loaded (token is empty)
	}
	// T_Not_Equals
	else if (tokenOld->type == T_Not_Equals) { 
		// INSTRUCTION_OPERATOR
		instruction->type = INSTRUCTION_OPERATOR;
		instruction->address_1 = malloc(sizeof(char)+1);
		char* Lower_Equals = "!=";
		strcpy(instruction->address_1, Lower_Equals);
		instruction_insert_last(list, instruction);
		return ERROR_OK; // token is not loaded (token is empty)
	}
	else 
		return ERROR_PARSER_SYNTAX;

	// token is not loaded (token is empty)
}

/**
 * @brief      Function body
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 * 
 * 3 parts:  
 * <function_body> -> <compoud_command> // compoud commands (commands without definitions)
 * <function_body> -> <type> T_ID <end_part_variable> // definitions
 * <function_body> -> <command> // commands (0-N, for example: IF, WHILE, initialization...)
 */
TError sim_function_body(
	TInstruction * instruction, 
	TInstructionList *list, 
	TToken* tokenNew, 
	TToken *tokenOld, 
	TInputSource * File)
{
	TError error_state = ERROR_PARSER_SYNTAX;
	tDataType type;
	// token is allready loaded
	
	// <function_body> -> <compoud_command>
	while (tokenOld->type == T_Left_BBracket) { // T_Left_BBracket -> call <sim_compoud_command>
		if ((error_state = sim_compoud_command(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK ) // <sim_compoud_command>
			return error_state;
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state; //pre load for class
	}

	// if
	// <command> -> T_If T_Left_Bracket <statement> T_Right_Bracket <compoud_command> T_Else <compoud_command> <function_body>
	if (tokenOld->type == T_If) {

		// INSTRUCTION_IF
		instruction->type = INSTRUCTION_IF;
		instruction_insert_last(list, instruction);

		// T_Left_Bracket
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state; 
		if (tokenOld->type != T_Left_Bracket)
			return ERROR_PARSER_SYNTAX;
		
		// <statement>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;  
		if ((error_state = sim_statement(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;

		// token is allready loaded (token is full)

		// T_Right_Bracket
		if (tokenOld->type != T_Right_Bracket)
			return ERROR_PARSER_SYNTAX;

		// <sim_compoud_command>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;
		if ((error_state = sim_compoud_command(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK ) 
			return error_state;
		// token is allready loaded (token is full)

		// T_Else
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state; 
		if (tokenOld->type != T_Else)
			return ERROR_PARSER_SYNTAX;

		// INSTRUCTION_ELSE
		instruction->type = INSTRUCTION_ELSE;
		instruction_insert_last(list, instruction);

		// <sim_compoud_command>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state; 
		if ((error_state = sim_compoud_command(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK ) 
			return error_state;
		// token is allready loaded (token is full)

		// INSTRUCTION_END_IF
		instruction->type = INSTRUCTION_END_IF;
		instruction_insert_last(list, instruction);

		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;		
		if ((error_state = sim_function_body(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
			return error_state;
		else	
			return ERROR_OK; // token is allready loaded (token is full)
	}

	// while
	// <command> -> T_While T_Left_Bracket <statement> T_Right_Bracket <compoud_command> <function_body>	
	else if (tokenOld->type == T_While) {

		// INSTRUCTION_WHILE
		instruction->type = INSTRUCTION_WHILE;
		instruction_insert_last(list, instruction);

		// T_Left_Bracket
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state; 
		if (tokenOld->type != T_Left_Bracket)
			return ERROR_PARSER_SYNTAX;
		
		// <statement>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state; 
		if ((error_state = sim_statement(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;

		// token is allready loaded (token is full)

		// T_Right_Bracket
		if (tokenOld->type != T_Right_Bracket)
			return ERROR_PARSER_SYNTAX;

		// <sim_compoud_command>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state; 
		if ((error_state = sim_compoud_command(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK ) 
			return error_state;
		// token is allready loaded (token is full)

		// INSTRUCTION_END_WHILE
		instruction->type = INSTRUCTION_END_WHILE;
		instruction_insert_last(list, instruction);

		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state; // pre-load
		if ((error_state = sim_function_body(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
			return error_state;
		else	
			return ERROR_OK; // token is allready loaded (token is full)
	}

	// return
	// <command> -> T_Return <statement> T_Semicolon
	// <command> -> T_Return T_Semicolon	
	else if (tokenOld->type == T_Return) {

		// INSTRUCTION_RETURN
		instruction->type = INSTRUCTION_RETURN;
		instruction->flag = TRUE;

		// T_Semicolon || <statement>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state; 
		if (tokenOld->type == T_Semicolon) {
			
			// INSTRUCTION_RETURN
			instruction->flag = FALSE;
			instruction_insert_last(list, instruction);

			if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
				return error_state; 
			if ((error_state = sim_function_body(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
				return error_state;
			else	
				return ERROR_OK; // token is allready loaded (token is full)
		}
		else if ((error_state = sim_statement(instruction, list, tokenNew, tokenOld, File)) == ERROR_OK) {
				
			// token is allready loaded (token is full)

			// T_Semicolon
			if (tokenOld->type != T_Semicolon)
				return ERROR_PARSER_SYNTAX;
			else {
				if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
					return error_state; 				
				if ((error_state = sim_function_body(instruction, list, tokenNew, tokenOld, File)) == ERROR_OK )
					return error_state;
				else	
					return ERROR_OK; // token is allready loaded (token is full)
			}
		}
		else 
			return error_state;
							
	}

	// initialization / assignment || call function (four way, how call)
	// <command> -> T_ID T_Equals <statement> T_Semicolon <function_body>
	//
	// <command> -> T_ID T_Equals T_ID T_Dot T_ID T_Left_Bracket <params> T_Right_Bracket T_Semicolon <function_body>
	// <command> -> T_ID T_Equals T_ID T_Left_Bracket <params> T_Right_Bracket T_Semicolon <function_body> 
	// <command> -> T_ID T_Dot T_ID T_Left_Bracket <params> T_Right_Bracket T_Semicolon <function_body>
	// 
	// <command> -> T_ID T_Left_Bracket <params> T_Right_Bracket T_Semicolon <function_body>
	
	// T_ID
	else if (tokenOld->type == T_ID_Simple || tokenOld->type == T_ID_Full) {

		// INSTRUCTION_ASSIGNMENT
		instruction->type = INSTRUCTION_ASSIGNMENT;
		instruction->address_1 = copy_data(tokenOld);
		
		// must have tmp_instruction, to save unaccomplished instruction
		TInstruction * tmp_instruction;
		tmp_instruction = malloc(sizeof(TInstruction));
		tmp_instruction->address_1 = instruction->address_1;



		// T_Equals || T_Dot || T_Left_Bracket
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;

		// T_Equals
		if (tokenOld->type == T_Equals) {	 

			free(tmp_instruction);

			// INSTRUCTION_ASSIGNMENT
			instruction_insert_last(list, instruction);

			// <statement>
			if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
				return error_state;  	                                                          
			if ((error_state = sim_statement(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
				return error_state;

			// token is allready loaded (token is full)

			// T_Semicolon
			if (tokenOld->type != T_Semicolon)
				return ERROR_PARSER_SYNTAX;
			
			// <function_body>
			if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
				return error_state; 				
			if ((error_state = sim_function_body(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
				return error_state;
			else	
				return ERROR_OK; // token is allready loaded (token is full)

		}
		// T_Left_Bracket - call function
		else if (tokenOld->type == T_Left_Bracket) {
			
			//INSTRUCTION_CALL_FUNCTION
			instruction->type = INSTRUCTION_CALL_FUNCTION;
			instruction->address_1 = tmp_instruction->address_1; // before token from tmp
			free(tmp_instruction);
			instruction_insert_last(list, instruction);

			// <params>
			if ((error_state = sim_params_call_function(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
				return error_state;

			// T_Right_Bracket
			if (tokenOld->type != T_Right_Bracket)
				return ERROR_PARSER_SYNTAX;

			// T_Semicolon
			if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
				return error_state;
			if (tokenOld->type != T_Semicolon)
				return ERROR_PARSER_SYNTAX;

			// <function_body>
			if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
				return error_state; 
			if ((error_state = sim_function_body(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
				return error_state;
			else	
				return ERROR_OK; // token is allready loaded (token is full)
		}

		// bad token
		else
			return ERROR_PARSER_SYNTAX;
	}

	// definitions || <command> -> epsilon
	else {

		// <function_body> -> <type> T_ID <end_part_variable> // definitions
		if (sim_type_without_void(instruction, list, tokenNew, tokenOld, File) == ERROR_OK) {

			tmpht = htSearch(ptrht, activeClass);
			tmpht = htSearch(tmpht->data, activeFunction);

			if (tokenOld->type == T_Int){
				instruction->var_type = TYPE_INT;
				type = INTEGER;
			}
			if (tokenOld->type == T_Double){
				instruction->var_type = TYPE_DOUBLE;
				type = DOUBLE;
			}
			if (tokenOld->type == T_String){
				instruction->var_type = TYPE_STRING;
				type = STRING;
			}
			
			// T_ID
			if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
				return error_state; 
			if (tokenOld->type != T_ID_Simple)
				return ERROR_PARSER_SYNTAX;
			
			char * varName = copy_data(tokenOld);
			htInsert(tmpht->param, varName, NULL, NULL, NULL, type);
			//INSTRUCTION_DECLARATION
			instruction->type = INSTRUCTION_DECLARATION;
			instruction->address_1 = copy_data(tokenOld);
			instruction_insert_last(list, instruction);
			instruction->var_type = TYPE_UNDEF;

			//free(varName);
			// <end_part_variable>
			if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
				return error_state; 			
			if ((error_state = sim_end_part_variable(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
				return error_state;
			else	
				return ERROR_OK; // token is allready loaded (token is full)						               
		}

		//<command> -> epsilon
		else
			return ERROR_OK; // token is allready loaded (token is full)
	}
		
}


/**
 * @brief      Varible - second part
 * 				(end part variable)
 *
 * @param      tokenNew  The token new
 * @param      tokenOld  The token old
 * @param      File      The file
 *
 * @return     TError
 *  
 * <end_part_variable> -> T_Semicolon <function_body>
 * <end_part_variable> -> T_Equals <statement> T_Semicolon <function_body>
 */
TError sim_end_part_variable(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File){

	TError error_state = ERROR_PARSER_SYNTAX;
	// token is allready loaded

	// T_Semicolon
	if (tokenOld->type == T_Semicolon) {
		
		// <function_body>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state; 				
		if ((error_state = sim_function_body(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
			return error_state;
		else	
			return ERROR_OK; // token is allready loaded (token is full)
	}
	// T_Equals
	else if (tokenOld->type == T_Equals) {

		// INSTRUCTION_ASSIGNMENT
		instruction->type = INSTRUCTION_ASSIGNMENT;
		instruction_insert_last(list, instruction);

		// <statement>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state;  	                                                          
		if ((error_state = sim_statement(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK)
			return error_state;

		// token is allready loaded (token is full)

		// T_Semicolon
		if (tokenOld->type != T_Semicolon)
			return ERROR_PARSER_SYNTAX;
		
		// <function_body>
		if((error_state = scanner_get_token(tokenOld, File)) != ERROR_OK)
			return error_state; 				
		if ((error_state = sim_function_body(instruction, list, tokenNew, tokenOld, File)) != ERROR_OK )
			return error_state;
		else	
			return ERROR_OK; // token is allready loaded (token is full)		
	}
}
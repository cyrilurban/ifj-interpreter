/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of embeded functions
 */

#include "embeded.h"

/** print() function will be part of interpreter module. */

/**
 * @brief Reads integer from standard input until EOL or EOF.
 *
 * @param int* number Loaded integer.
 *
 * @return TError ERROR_OK. ERROR_INTERPRETER_LOAD_NUMBER if loaded number does not match integer literal. For other error states see helper_read_string() and helper_string_to_integer().
 */
TError embeded_read_int(int *number)
{
	TError error_state = ERROR_OK;
	char *string = NULL;

	error_state = helper_read_string(&string, stdin, "\n", false);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	error_state = helper_string_to_integer(number, string);

	if (error_state == ERROR_HELPER_NO_MATCH) {
		return ERROR_INTERPRETER_LOAD_NUMBER;
	
	} else if (error_state != ERROR_OK) {
		return error_state;
	}

	return ERROR_OK;
}

/**
 * @brief Reads double from standard input until EOL or EOF.
 *
 * @param double* number Loaded double.
 *
 * @return TError ERROR_OK. ERROR_INTERPRETER_LOAD_NUMBER if loaded number does not match floating-point literal. For other error states see helper_read_string() and helper_string_to_double().
 */
TError embeded_read_double(double *number)
{
	TError error_state = ERROR_OK;
	char *string = NULL;

	error_state = helper_read_string(&string, stdin, "\n", false);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	error_state = helper_string_to_double(number, string);

	if (error_state == ERROR_HELPER_NO_MATCH) {
		return ERROR_INTERPRETER_LOAD_NUMBER;
	
	} else if (error_state != ERROR_OK) {
		return error_state;
	}

	return ERROR_OK;
}

/**
 * @brief Reads string from standard input until EOL or EOF.
 *
 * @param char* string Loaded string.
 *
 * @return TError ERROR_OK. For other error states see helper_read_string().
 */
TError embeded_read_string(char *string)
{
	TError error_state = ERROR_OK;
	char *temporary_string = NULL;

	error_state = helper_read_string(&temporary_string, stdin, "\n", false);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	string = temporary_string;

	return ERROR_OK;
}

/**
 * @brief Calculates the length of the string.
 *
 * @param char* string
 *
 * @return int The number of bytes in the string excluding the terminating null byte ('\0').
 */
int embeded_length(char *string)
{
	return (int) strlen(string);
}

/**
 * @brief Creates the substring from the string according to the boundaries (begin_index ans substring_length).
 *
 * @param char** substring
 * @param char* string
 * @param int begin_index
 * @param int substring_length
 *
 * @return TError ERROR_INTERPRETER_OTHER if the substring definition is out of boundaries of the string.
 * For other error states see helper_allocate_memmory(). ERROR_OK otherwise.
 */
TError embeded_substr(char **substring, char *string, int begin_index, int substring_length)
{
	TError error_state = ERROR_OK;
	int string_length = strlen(string);
	int substring_length_full = substring_length + 1; // + 1 for terminating null byte ('\0')

	// Check begin_index parameter
	if ((begin_index < 0) || (begin_index > string_length)) {
		return ERROR_INTERPRETER_OTHER;
	}

	// Check substring_length parameter
	if ((substring_length > string_length)
		|| ((begin_index + substring_length) > string_length)
	) {
		return ERROR_INTERPRETER_OTHER;
	}

	// Initialize substring for right allocation variant
	*substring = NULL;
	
	error_state = helper_allocate_memmory(
		(void **) substring,
		0, // Substring has no memmory yet
		substring_length_full,
		sizeof(char)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	*substring = strncpy(*substring, string + begin_index, substring_length);
	(*substring)[substring_length] = '\0';

	return ERROR_OK;
}

/**
 * @brief Compares the two strings.
 *
 * @param char* string_1
 * @param char* string_2
 *
 * @return int -1 if string_1 is less than, 1 if string_1 is greater than, or 0 if string_1 match string_2.
 */
int embeded_compare(char *string_1, char *string_2)
{
	int comparison = strcmp(string_1, string_2);

	// string_1 is less than string_2
	if (comparison < 0) {
		return -1;

	// string_1 is greater than string_2
	} else if (comparison > 0) {
		return 1;
	}

	// string_1 is equal to string_2
	return 0;
}

/**
 * @brief Finds the index of the first occurrence of the search pattern in the string.
 *
 * @param char* string
 * @param char* search
 *
 * @return int The index of character where the first occurrence of the search pattern was found, -1 otherwise.
 *
 * @todo Call search function from IAL module.
 */
int embeded_find(char *string, char *search)
{
	int string_length = strlen(string);
	int search_length = strlen(search);

	// Empty search pattern has default position
	if (search_length == 0) {
		return 0;

	// Search pattern cannot be found
	} else if (search_length > string_length) {
		return -1;
	}

	// return ial_boyer_moore(string, search);

	return -1;
}

/**
 * @brief Sorts characters of the string.
 *
 * @param char* sorted
 * @param char* string
 *
 * @return TError For error states see helper_allocate_memmory(). ERROR_OK otherwise.
 *
 * @todo Call sort function from IAL module.
 */
TError embeded_sort(char *sorted, char *string)
{
	TError error_state = ERROR_OK;
	int string_length_full = strlen(string) + 1; // + 1 for terminating null byte ('\0')

	// Initialize sorted for right allocation variant
	sorted = NULL;

	error_state = helper_allocate_memmory(
		(void **) &sorted,
		0, // Sorted has no memmory yet
		string_length_full,
		sizeof(char)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	sorted = strncpy(sorted, string, string_length_full);

	// ial_list_merge_sort(sorted);

	return ERROR_OK;
}

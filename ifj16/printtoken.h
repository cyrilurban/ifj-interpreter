/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/*
 * Header file of implementation print token function
 */

#ifndef PRINTTOKEN_H
#define PRINTTOKEN_H

#include <stdio.h>
#include <stdbool.h>
#include "error.h"
#include "scanner.h"

/**
 * @brief Prints token content.
 *
 * @param TToken* token
 * 
 */
void printtoken_print(TToken *token);

/**
 * @brief Prints all tokens from given source.
 *
 * @param TInputSource* source
 */
void printtoken_print_all(TInputSource *source);

#endif
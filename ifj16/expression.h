/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of expression solving.
 */

#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"
#include "ial.h"
#include "interpreter_stack.h"

/**
 * @brief Gets operator priority.
 *
 * @param char* string
 *
 * @return int Lower number means higher priority.
 *     -1 In case of undefined operator.
 */
int expression_get_operator_priority(char *string);

/**
 * @brief Converts expression from infix to postfix notation.
 *
 * @param TInterpreterStack* postfix_expression
 * @param TInterpreterStack* infix_expression
 *
 * @return TError ERROR_OK. For other error state see interpreter_init_stack() and interpreter_push_stack() and interpreter_push_inverted_stack().
 */
TError expression_infix_to_postfix(TInterpreterStack *postfix_expression, TInterpreterStack *infix_expression);


/**
 * @brief Solves expression.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStack* infix_expression
 * @param tHTable* function_symbol_table
 * @param tHTable* class_symbol_table
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 *     For other error states see interpreter_init_stack() and expression_infix_to_postfix().
 */
TError expression_solve(TInterpreterStackItem *result, TInterpreterStack *infix_expression, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);

/**
 * @brief Wrapper for loading indirect value
 *
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* function_symbol_table
 * @param tHTable* class_symbol_table
 * @param tHTable* symbol_table
 *
 * @return TError
 */
TError expression_load_indirect_value(TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);

/**
 * @brief Multiplies x and y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_multiply(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);

/**
 * @brief Divides x and y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_divide(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);

/**
 * @brief Adds x and y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_add(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);

/**
 * @brief Subtracts x and y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_subtract(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);

/**
 * @brief Tells if x is lower than y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_is_lower(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);


/**
 * @brief Tells if x is greater than y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_is_greater(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);

/**
 * @brief Tells if x is lower or equal to y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_is_lower_or_equal(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);

/**
 * @brief Tells if x is greater or equal to y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_is_greater_or_equal(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);

/**
 * @brief Tells if x is equal to y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_is_equal(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);

/**
 * @brief Tells if x is not equal to y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_is_not_equal(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table);

#endif

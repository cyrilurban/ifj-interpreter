/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of instruction list
 */

#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include <stdbool.h>
#include "error.h"
#include "helper.h"

/**
 * @brief Instruction types.
 */
typedef enum TInstructionType {
	INSTRUCTION_INIT = 0,
	INSTRUCTION_CLASS,
	INSTRUCTION_END_CLASS,
	INSTRUCTION_ASSIGNMENT,
	INSTRUCTION_DECLARATION,
	INSTRUCTION_OPERAND,
	INSTRUCTION_WHILE,
	INSTRUCTION_END_WHILE,
	INSTRUCTION_IF,
	INSTRUCTION_END_IF,
	INSTRUCTION_ELSE,
	INSTRUCTION_FUNCTION,
	INSTRUCTION_PARAMS,
	INSTRUCTION_LEFT_BRACKET,
	INSTRUCTION_RIGHT_BRACKET,
	INSTRUCTION_START_STATEMENT,
	INSTRUCTION_END_STATEMENT,
	INSTRUCTION_OPERATOR,
	INSTRUCTION_CALL_FUNCTION,
	INSTRUCTION_RETURN

} TInstructionType;

/**
 * @brief Instruction types.
 */
typedef enum TInstructionFlag {
	FALSE = 0,
	TRUE = 1
} TInstructionFlag;

typedef enum TInstructionVariable {
	TYPE_UNDEF = 0,
	TYPE_INT,
	TYPE_DOUBLE,
	TYPE_STRING,
	TYPE_QUOTED_STRING,
	TYPE_ID_SIMPLE,
	TYPE_ID_FULL,
	TYPE_VOID

} TInstructionVariable;
/**
 * @brief Instruction structure.
 */
typedef struct TInstruction {
	TInstructionType type;
	TInstructionFlag flag;
	TInstructionVariable var_type ;
	void *address_1;
	void *address_2;
	void *address_3;
} TInstruction;

/**
 * @brief Instruction list node structure.
 */
typedef struct TInstructionListNode {
	TInstruction *data;
	struct TInstructionListNode *next;
} TInstructionListNode;

/**
 * @brief Instruction list structure.
 */
typedef struct TInstructionList {
	TInstructionListNode *first;
	TInstructionListNode *last;
	TInstructionListNode *active;
} TInstructionList;

/**
 * Initializes instruction list.
 *
 * @param TInstructionList** list
 *
 * @return TError ERROR_OK. For error states see helper_allocate_memmory().
 */
TError instruction_init_list(TInstructionList **list);

/**
 * Clears instruction list.
 *
 * @param TInstructionList** list
 */
void instruction_clear_list(TInstructionList **list);

/**
 * Creates new node from given data and inserts it to the end of instruction list.
 * 
 * @param TInstructionList* list
 * @param TInstructionType type
 * @param TInstructionFlag flag
 * @param void* address_1
 * @param void* address_2
 * @param void* address_3
 *
 * @return TError ERROR_OK. For error states see helper_allocate_memmory().
 */
TError instruction_insert_last(
	TInstructionList *list,
	TInstruction * instruction
);

/**
 * Activates next or the first instruction.
 * 
 * @param TInstructionList* list
 */
void instruction_activate_next(TInstructionList *list);

/**
 * @brief Rewinds to the given instruction.
 *
 * @param TInstructionList* list
 * @param TInstruction* instruction
 */
void instruction_rewind(TInstructionList *list, TInstruction *instruction);

/**
 * @brief Rewinds before the given instruction.
 *
 * @param TInstructionList* list
 * @param TInstruction* instruction
 */
void instruction_rewind_before(TInstructionList *list, TInstruction *instruction);

/**
 * Reads data of active instruction.
 * 
 * @param TInstructionList* list
 *
 * @return TInstruction Data of active instruction, NULL otherwise.
 */
TInstruction *instruction_read(TInstructionList *list);

/**
 * Tells if the list is empty or not.
 *
 * @param TInstructionList* list
 *
 * @return bool True if the list is empty, false otherwise.
 */
bool instruction_is_empty(TInstructionList *list);


/**
 * @brief      Print instruction list
 *
 * @param      instruction  The instruction
 */
void instruction_print(TInstruction *instruction);


/**
 * @brief      Prints all tokens on instruction list (starts from active)
 *
 * @param      list         The list
 */
void instruction_print_all(TInstructionList *list);

#endif

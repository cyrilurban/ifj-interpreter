/**
 * Implementation of IAL algorithms
 */

#ifndef IAL_H
#define IAL_H

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "instruction.h"
 
//velkost abecedy pouzivanej v Boyer-Moorovom algoritme 
#define  MAX_ALPHABET_SIZE 256
//hodnota musi byt prvocislo
#define  HASH_TABLE_SIZE 107 


typedef char* tKey;
typedef void* tData;

typedef enum { 
	T_VOID,
	INTEGER,
	STRING,
	DOUBLE,
	PARAM_INTEGER,
	PARAM_STRING,
	PARAM_DOUBLE,
	HASH_TABLE,
	FUNCTION,
}tDataType;

typedef struct tHTItem{
	tKey key;
	tData data;	
	tData param;
	tDataType data_type;
	struct tHTItem* ptrnext;
	TInstructionList *list;
} tHTItem;


struct list
{
    struct list* next;
    int data;
};

typedef tHTItem* tHTable[HASH_TABLE_SIZE];

//hlavickove subory k Boyer-Moorovmu algorytmu
void computeJumps(int array_jump[], char *pattern);

void computeMatchJumps(int array_match[], char *pattern);

int BMA(char *string, char *pattern);


//hlavickove subory k list Merge Sort
struct list* sortMerge(struct list* a, struct list* b);

void frontBackSplit(struct list* source, struct list** frontRef, struct list** backRef);

void listMergeSort(struct list** headRef);



//hlavickove subory k TRP
int hashCode(tKey key);

void htInit(tHTable* ptrht);

tHTItem* htSearch(tHTable* ptrht, tKey key);

void htInsert(tHTable* ptrht, tKey key, tData data, tData param, TInstructionList *list, tDataType data_type);

tData* htRead(tHTable* ptrht, tKey key);

void htDelete(tHTable* ptrht, tKey key);

void htClearAll(tHTable* ptrht);


#endif


/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of general functions
 */

#include "helper.h"

/**
 * @brief Allocates new or extra memmory chunk for the target.
 *
 * @param void** target If *target has NULL value malloc() will be called, realloc() otherwise.
 * @param int target_size Number of elements pointed by target.
 * @param int needed_size Number of elements to be allocated of reallocated. If less than zero default value is HELPER_ALLOCATE_MEMMORY_SIZE.
 * @param int sizeof_type Output of sizeof(element type).
 *
 * @return TError ERROR_INTERNAL if parameter have bad format or memmory cannot be allocated, ERROR_OK otherwise.
 */
TError helper_allocate_memmory(void **target, int target_size, int needed_size, int sizeof_type)
{
	void *temporary_target = NULL;

	// Check target_size parameter
	if (target_size < 0) {
		return ERROR_INTERNAL;
	}

	// Check needed_size parameter
	if (needed_size < 0) {
		needed_size = HELPER_ALLOCATE_MEMMORY_SIZE;
	}

	// Reallocate memmory for non empty target.
	if (*target != NULL) {
		temporary_target = (void *) realloc(*target, (target_size + needed_size) * sizeof_type);
		
		if (temporary_target == NULL) {
			return ERROR_INTERNAL;
		}

		*target = temporary_target;

	// Allocate memmory for fresh target.
	} else {
		temporary_target = (void *) malloc(needed_size * sizeof_type);

		if (temporary_target == NULL) {
			return ERROR_INTERNAL;
		}

		*target = temporary_target;
	}

	return ERROR_OK;
}

/**
 * @brief Reads string from the source to the first end character or to EOF. Function inserts terminating null byte ('\0').
 *
 * @param char** string
 * @param FILE* source
 * @param char* end_characters List of end characters terminated by null byte ('\0').
 * @param bool include_end_character If true function includes end character into the string.
 *
 * @return TError ERROR_OK. For other error states see helper_allocate_memmory().
 */
TError helper_read_string(char **string, FILE *source, char *end_characters, bool include_end_character)
{
	TError error_state = ERROR_OK;
	int c;
	int i = 0;
	int available_length = 0;
	int string_length = 0;
	int end_characters_length = 0;
	bool read = true;

	if (end_characters != NULL) {
		end_characters_length = strlen(end_characters);
	}

	while (read) {

		// Allocate memmory
		if (available_length == string_length) {
			error_state = helper_allocate_memmory(
				(void **) string,
				string_length,
				-1, // Use default value
				sizeof(char)
			);

			if (error_state != ERROR_OK) {
				return error_state;
			}

			available_length += HELPER_ALLOCATE_MEMMORY_SIZE;
		}

		c = fgetc(source);

		// Test if loaded character is end character
		if (end_characters != NULL) {
		
			for (i = 0; i < end_characters_length; i++) {
				
				if (c == end_characters[i]) {
					read = false;
					break;
				}
			}
		}

		if (c == EOF) {
			read = false;
		}

		if (read || include_end_character) {
			(*string)[string_length] = c;
			string_length++;
		}
	}

	// Align memmory and make space for terminating null byte
	error_state = helper_allocate_memmory(
		(void **) string,
		string_length,
		1, // 1 for terminating null byte ('\0')
		sizeof(char)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	(*string)[string_length] = '\0';

	return ERROR_OK;
}

/**
 * @brief Tells if the string matches the pattern.
 *
 * @param char* string
 * @param char* pattern Extended POSIX regular expression.
 *
 * @return TError ERROR_HELPER_MATCH if the string matches the pattern, ERROR_HELPER_NO_MATCH if it does not. ERROR_INTERNAL otherwise.
 */
TError helper_match_pattern(char *string, char *pattern)
{
	regex_t regex;
	int state;

	state = regcomp(&regex, pattern, REG_EXTENDED | REG_ICASE);

	if (state != 0) {
		return ERROR_INTERNAL;
	}

	state = regexec(&regex, string, 0, NULL, 0);
	regfree(&regex);

	if (state == 0) {
		return ERROR_HELPER_MATCH;
	
	} else if (state == REG_NOMATCH) {
		return ERROR_HELPER_NO_MATCH;
	}

	return ERROR_INTERNAL;
}

/**
 * @brief Converts the string containing integer literal to integer.
 *
 * @param int* number
 * @param char* string
 *
 * @return TError ERROR_OK. ERROR_INTERNAL if converted number is out of range. For other error states see helper_match_pattern().
 */
TError helper_string_to_integer(int *number, char *string)
{
	TError error_state = ERROR_OK;
	long int temporary_number;

	error_state = helper_match_pattern(string, "^([0-9]+)$");

	if (error_state != ERROR_HELPER_MATCH) {
		return error_state;
	}
	
	temporary_number = strtol(string, NULL, 10);

	// Check number range
	if ((temporary_number < INT_MIN) || (temporary_number > INT_MAX)) {
		return ERROR_INTERNAL;
	}

	*number = (int) temporary_number;

	return ERROR_OK;
}

/**
 * @brief Converts the string containing floating-point literal to double.
 *
 * @param double* number
 * @param char* string
 *
 * @return TError ERROR_OK. ERROR_INTERNAL if converted number is out of range. For other error states see helper_match_pattern().
 */
TError helper_string_to_double(double *number, char *string)
{
	TError error_state = ERROR_OK;
	double temporary_number;

	error_state = helper_match_pattern(string, "^([0-9]+)(((\\.[0-9]+)|(e[\\+\\-]?[0-9]+))|((\\.[0-9]+)(e[\\+\\-]?[0-9]+)))$");

	if (error_state != ERROR_HELPER_MATCH) {
		return error_state;
	}
	
	temporary_number = strtod(string, NULL);

	// Check number range
	if ((errno == ERANGE)
		&& (temporary_number == HUGE_VAL) || (temporary_number == -HUGE_VAL)
	) {
		return ERROR_INTERNAL;
	}

	*number = temporary_number;

	return ERROR_OK;
}

/**
 * @brief Merges two strings.
 *
 * @param char** string
 * @param char* string_1
 * @param char* string_2
 *
 * @return TError
 */
TError helper_merge_string_string(char **string, char *string_1, char * string_2)
{
	TError error_state = ERROR_OK;
	int string_1_length = strlen(string_1);
	int string_2_length = strlen(string_2);
	int string_length_full = string_1_length + string_2_length + 1; // string length with terminating null byte ('\0')

	// Initialize for right allocation variant
	*string = NULL;

	error_state = helper_allocate_memmory(
		(void **) string,
		0, // String has no memmory
		string_length_full,
		sizeof(char)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	strncpy(*string, string_1, string_1_length);
	strncpy(*string + string_1_length, string_2, string_2_length);
	(*string)[string_length_full - 1] = '\0';

	return error_state;
}

/**
 * @brief Converts double to string.
 *
 * @param char** string
 * @param double number
 *
 * @return TError
 */
TError helper_double_to_string(char **string, double number)
{
	TError error_state = ERROR_OK;

	*string = NULL;

	error_state = helper_allocate_memmory(
		(void **) string,
		0, // String has no memmory
		-1, // Use default value
		sizeof(char)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	snprintf(*string, HELPER_ALLOCATE_MEMMORY_SIZE, "%g", number);

	return error_state;
}

/**
 * @brief Converts int to string.
 *
 * @param char** string
 * @param int number
 *
 * @return TError
 */
TError helper_integer_to_string(char **string, int number)
{
	TError error_state = ERROR_OK;

	*string = NULL;

	error_state = helper_allocate_memmory(
		(void **) string,
		0, // String has no memmory
		-1, // Use default value
		sizeof(char)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	snprintf(*string, HELPER_ALLOCATE_MEMMORY_SIZE, "%d", number);

	return error_state;
}

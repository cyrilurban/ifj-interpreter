/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of embeded functions
 */

#ifndef EMBEDED_H
#define EMBEDED_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "error.h"
#include "helper.h"
#include "ial.h"

/** @def Embeded function name prefix regex */
#define EMBEDED_FUNCTION_NAME_PREFIX_REGEX "ifj16\\."

/** @def Embeded function names */
#define EMBEDED_FUNCTION_NAME_READ_INT "ifj16.readInt"
#define EMBEDED_FUNCTION_NAME_READ_DOUBLE "ifj16.readDouble"
#define EMBEDED_FUNCTION_NAME_READ_STRING "ifj16.readString"
#define EMBEDED_FUNCTION_NAME_PRINT "ifj16.print"
#define EMBEDED_FUNCTION_NAME_LENGTH "ifj16.length"
#define EMBEDED_FUNCTION_NAME_SUBSTR "ifj16.substr"
#define EMBEDED_FUNCTION_NAME_COMPARE "ifj16.compare"
#define EMBEDED_FUNCTION_NAME_FIND "ifj16.find"
#define EMBEDED_FUNCTION_NAME_SORT "ifj16.sort"

/** print() function will be part of interpreter module. */

/**
 * @brief Reads integer from standard input until EOL or EOF.
 *
 * @param int* number Loaded integer.
 *
 * @return TError ERROR_OK. ERROR_INTERPRETER_LOAD_NUMBER if loaded number does not match integer literal. For other error states see helper_read_string() and helper_string_to_integer().
 */
TError embeded_read_int(int *number);

/**
 * @brief Reads double from standard input until EOL or EOF.
 *
 * @param double* number Loaded double.
 *
 * @return TError ERROR_OK. ERROR_INTERPRETER_LOAD_NUMBER if loaded number does not match floating-point literal. For other error states see helper_read_string() and helper_string_to_double().
 */
TError embeded_read_double(double *number);

/**
 * @brief Reads string from standard input until EOL or EOF.
 *
 * @param char* string Loaded string.
 *
 * @return TError ERROR_OK. For other error states see helper_read_string().
 */
TError embeded_read_string(char *string);

/**
 * @brief Calculates the length of the string.
 *
 * @param char* string
 *
 * @return int The number of bytes in the string excluding the terminating null byte ('\0').
 */
int embeded_length(char *string);

/**
 * @brief Creates the substring from the string according to the boundaries (begin_index ans substring_length).
 *
 * @param char** substring
 * @param char* string
 * @param int begin_index
 * @param int substring_length
 *
 * @return TError ERROR_INTERPRETER_OTHER if the substring definition is out of boundaries of the string.
 * For other error states see helper_allocate_memmory(). ERROR_OK otherwise.
 */
TError embeded_substr(char **substring, char *string, int begin_index, int substring_length);

/**
 * @brief Compares the two strings.
 *
 * @param char* string_1
 * @param char* string_2
 *
 * @return int -1 if string_1 is less than, 1 if string_1 is greater than, or 0 if string_1 match string_2.
 */
int embeded_compare(char *string_1, char *string_2);

/**
 * @brief Finds the index of the first occurrence of the search pattern in the string.
 *
 * @param char* string
 * @param char* search
 *
 * @return int The index of character where the first occurrence of the search pattern was found, -1 otherwise.
 */
int embeded_find(char *string, char *search);

/**
 * @brief Sorts characters of the string.
 *
 * @param char* sorted
 * @param char* string
 *
 * @return TError For error states see helper_allocate_memmory(). ERROR_OK otherwise.
 */
TError embeded_sort(char *sorted, char *string);

#endif

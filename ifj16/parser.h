/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of parser
 */
#include "error.h"
#include "scanner.h"
#include "helper.h"
#include "printtoken.h"
#include "instruction.h"
#include "interpreter.h"
#include "ial.h"

#ifndef PARSER_H
#define PARSER_H

#define TRUE 1
#define FALSE 0

TError parser(TInputSource* File);
TError sim_class(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_next_class(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_static_definitions(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_type(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_end_part_static(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_params_call_function(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_params_definitions(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_end_part_params_call_function(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_end_part_params_definitions(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_type_without_void(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_end_part_params(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_statement(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_end_part_statement(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_brackets(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_operand(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_operator(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_compoud_command(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_function_body(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);
TError sim_end_part_variable(TInstruction * instruction, TInstructionList *list, TToken* tokenNew, TToken *tokenOld, TInputSource * File);

void * copy_data(TToken* token);
void * copy_character(char c);

#endif
##
## Project makefile
##

BUILD_DIR='build'
DOCS_DIR='docs'
PACK_DIR='pack'
TESTS_DIR='tests'
SOURCE_DIR='ifj16'

PACK='xobrus00'
DOCUMENTATION='dokumentace.pdf'
EXTENSIONS='rozsireni'
MAKEFILE='Makefile_final'
MAKEFILE_TESTS='Makefile_tests_final'
POINTS='rozdeleni'
TESTS='run_tests.sh'

COMPILER=gcc
COMPILER_FLAGS=-std=c99 -pedantic -Wall -W

OUTPUT=ifj16
SOURCES=$(shell find $(SOURCE_DIR) -name '*.c')
OBJECTS=$(patsubst %.c, %.o, $(SOURCES))

BUILD_PATH=$(BUILD_DIR)
OUTPUT_PATH=$(BUILD_DIR)/$(OUTPUT)
OBJECTS_PATH=$(SOURCE_DIR)/*.o
DEPENDENCIES_PATH=$(SOURCE_DIR)/*.d


$(OUTPUT): $(OBJECTS)
	test -d $(BUILD_PATH) || mkdir $(BUILD_PATH)
	$(COMPILER) $(COMPILER_FLAGS) $(OBJECTS) -o $(OUTPUT_PATH)

-include $(OBJECTS:.o=.d)

$(SOURCE_DIR)/%.o: $(SOURCE_DIR)/%.c
	$(COMPILER) $(COMPILER_FLAGS) -c $*.c -o $*.o
	$(COMPILER) $(COMPILER_FLAGS) -MM $*.c > $*.d

clean:
	rm -fr $(BUILD_PATH) $(OBJECTS_PATH) $(DEPENDENCIES_PATH) $(PACK_DIR)

test:
	sh $(TESTS)

packdir:
	test -d $(PACK_DIR) || mkdir $(PACK_DIR)

pack: clean packdir
	cp -r $(SOURCE_DIR)/. $(PACK_DIR)
	cp -T $(DOCS_DIR)/$(DOCUMENTATION) $(PACK_DIR)/$(DOCUMENTATION)
	cp -T $(EXTENSIONS) $(PACK_DIR)/$(EXTENSIONS)
	cp -T $(MAKEFILE) $(PACK_DIR)/Makefile
	cp -T $(POINTS) $(PACK_DIR)/$(POINTS)
	cd $(PACK_DIR) && zip -r $(PACK) * && rm -fr *.c *.h $(DOCUMENTATION) $(EXTENSIONS) Makefile $(POINTS)

packtests: clean packdir
	cp -r $(SOURCE_DIR)/. $(PACK_DIR)
	cp -r $(TESTS_DIR) $(PACK_DIR)
	cp -T $(DOCS_DIR)/$(DOCUMENTATION) $(PACK_DIR)/$(DOCUMENTATION)
	cp -T $(EXTENSIONS) $(PACK_DIR)/$(EXTENSIONS)
	cp -T $(MAKEFILE_TESTS) $(PACK_DIR)/Makefile
	cp -T $(POINTS) $(PACK_DIR)/$(POINTS)
	cp -T $(TESTS) $(PACK_DIR)/$(TESTS)
	cd $(PACK_DIR) && zip -r $(PACK) * && rm -fr *.c *.h $(DOCUMENTATION) $(EXTENSIONS) Makefile $(POINTS) $(TESTS) $(TESTS_DIR)

